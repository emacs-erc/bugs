#!/bin/bash
set -e

#                                                     shellcheck disable=SC2154
[[ $CI_PROJECT_DIR ]]

patch_dir=$1

[[ -d $patch_dir ]]

git config --global user.email "you@example.com"
git config --global user.name "Your Name"
git -C /checkout checkout -b mine
readarray -t patches < <(
    export LANG=en_US.UTF-8
    export LC_ALL=C
    find "$(realpath "$patch_dir")" -name \*.patch | sort -n
)
rm -vf /checkout/lisp/erc/*.elc
git -C /checkout am "${patches[@]}"
git -C /checkout --no-pager log -n${#patches[@]}
