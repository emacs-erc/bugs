#!/bin/bash
set -e

thisfile=$(realpath -e "${BASH_SOURCE[0]}")
thisdir=${thisfile%/*}
rootdir=${thisdir%/*}

: "${ORAGONO_VERSION:=v2.11.1}"
: "${ZNC_VERSION:=1.8-slim}"
: "${SOJU_VERSION:=v0.7.0}"
: "${INSPIRCD_VERSION:=3.17}"
: "${ATHEME_VERSION:=7.2}"

if [[ -e $rootdir/.env ]]; then
    #                                                 shellcheck disable=SC1090
    source "$rootdir/.env"
fi

[[ $DATADIR ]]
mkdir -p "$DATADIR"
if [[ ! -f $rootdir/resources/bots/shakes.txt ]]; then
    (cd "$rootdir/resources/bots"; tar -xf shakes.txt.tar.gz)
fi

zncdata=$DATADIR/znc
sjudata=$DATADIR/soju
ogodata=$DATADIR/oragono
botdata=$DATADIR/bots
insdata=$DATADIR/inspircd
athdata=$DATADIR/atheme
pncdata=$DATADIR/pounce

pod_pubarg_defaults=(
    --publish "127.0.0.1:6667:6667"
    --publish "127.0.0.1:6668:6668"
    --publish "127.0.0.1:6670:6670"
)
rootless=$(id -u)
pun=()
if (( rootless )); then
    pun+=( podman unshare )
fi

create_pod() {
    (( $# )) || set -- "${pod_pubarg_defaults[@]}"
    podman pod create --name irc "$@"
}

create_oragono() {
    local name port tls_port ws_port
    (( $# == 2 ))
    name=$1
    [[ $name != *"|"* ]]
    [[ $name != *net* ]]
    port=$2
    (( tls_port = port + 30 ))
    (( ws_port = port + 1430 ))
    data=$ogodata/net-$name
    rm -rf "$data"
    mkdir -p "$data"
    sed -e "s/@IPV4_LISTEN@/:$port/" \
        -e "s/@TLS_PORT@/$tls_port/" \
        -e "s/@WS_PORT@/$ws_port/" \
        -e 's|@PATH_LANGUAGES@|/ircd-bin/languages|' \
        -e "s|@NETWORK_NAME@|${name}net|" \
        "$rootdir/resources/oragono/default.yaml.in" > "$data/ircd.yaml"
    podman create \
        --pod irc \
        --name "net-$name" \
        --log-driver json-file \
        --no-hosts \
        --volume "$data:/ircd:z" \
        "ghcr.io/ergochat/ergo:$ORAGONO_VERSION"
}

pick_lines() {
    local i m
    local max file
    file=$1
    m=$(wc -l < "$file")
    max=${2:-$(( m / 10 ))}
    for (( i = 1 ; i < max + 1 ; i++ )); do
        (( n = RANDOM % m ))
        sed -n "${n}p" "$file"
    done
}

span_lines() {
    local s m n d
    local beg end
    file=$1
    [[ -r $file ]]
    n=$2
    d=$3
    m=$(wc -l < "$file")
    (( s = m / d ))
    (( beg = s * (n - 1) )) || (( beg = 1 ))
    (( end = s * n - 1 ))
    sed -n "${beg},${end}p" "$file"
}

create_ircbot() {
    (( $# > 3 ))
    local name port chan chans
    name=$1
    port=$2
    lbeg=${3%/*}
    lend=${3#*/}
    shift; shift; shift
    data=${botdata:?/tmp/fake}/$name
    rm -rf "$data"
    mkdir -p "$data"
    for chan in "$@"; do
        chans+=$'    ${#}'"$chan"$'\\\n'
    done
    sed -e "s/@NAME@/$name/" \
        -e "s/@PORT@/$port/" \
        -e 's/@CHANS@/'"$chans"/ \
        "$rootdir/resources/bots/config.ini.in" > "$data/config.ini"
    cp "$rootdir/resources/bots/${BOT_TYPE:-bot}.py" "$data/bot.py"
    span_lines "$rootdir/resources/bots/shakes.txt" \
        "$lbeg" "$lend" > "$data/human.db"
    "${pun[@]}" chown 1000:1000 "$data/human.db"
    podman create \
        --pod irc \
        --name "bot-$name" \
        --log-driver json-file \
        --volume "$data:/usr/src/bot:Z" \
        docker.io/gawel/irc3:latest
}

# $1, if present must be a znc-data directory tree to clone
# Symlinks will be dereferenced.
create_znc() {
    "${pun[@]}" rm -rf "${zncdata:-/tmp/fake}"
    "${pun[@]}" cp -rL "${1:-$rootdir/resources/znc}" "$zncdata"
    "${pun[@]}" test -f "$zncdata/configs/znc.conf"
    podman create \
        --pod irc \
        --name znc \
        --log-driver json-file \
        --volume "$zncdata:/znc-data:Z" \
        "docker.io/library/znc:$ZNC_VERSION" \
        --debug
}

create_soju() {
    "${pun[@]}" rm -rf "${sjudata:-/tmp/fake}"
    "${pun[@]}" cp -rL "${1:-$rootdir/resources/soju}" "$sjudata"
    "${pun[@]}" test -f "$sjudata/config"
    echo changeme | podman run --rm -i \
        --volume "$sjudata:/etc/soju:Z" \
        --volume "$sjudata:/var/lib/soju" \
        "localhost/soju:$SOJU_VERSION" sojudb \
        -config /etc/soju/config create-user tester -admin

    podman run \
        --detach \
        --pod irc \
        --name soju \
        --log-driver json-file \
        --volume "$sjudata:/etc/soju:Z" \
        --volume "$sjudata:/var/lib/soju:Z" \
        "localhost/soju:$SOJU_VERSION" \
        soju -debug -config /etc/soju/config
    podman exec soju sojuctl -config /etc/soju/config \
           user run tester \
           network create -addr irc+insecure://localhost:6667 -name foonet
    podman exec soju sojuctl -config /etc/soju/config \
           user run tester \
           network create -addr irc+insecure://localhost:6668 -name barnet
    podman stop soju
}

create_pounce() {
    local name port data
    [[ $1 ]]
    name=${1:-net-foo} # must match known oragono name
    [[ -e $ogodata/$name/fullchain.pem ]]
    data=$pncdata/$name
    [[ ! $2 || $2 =~ ^[[:digit:]]+$ ]]
    port=${2:-6697}
    "${pun[@]}" rm -rf "${data:-/tmp/fake}"
    "${pun[@]}" cp -rL "$rootdir/resources/pounce" "$data"
    "${pun[@]}" test -f "$data/local.conf"
    "${pun[@]}" cp "$ogodata/$name/fullchain.pem" "$data/127.0.0.1.pem"
    "${pun[@]}" cp "$ogodata/$name/privkey.pem" "$data/127.0.0.1.key"
    podman create \
        --pod irc \
        --name pounce \
        --log-driver json-file \
        --volume "$data:/etc/xdg/pounce:z" \
        "localhost/pounce:latest" \
        pounce -n tester -h localhost -p "$port" local.conf
}

# s3cret
inspass='cNkbWRWn$MhSTITMbrCxp0neoDqL66/MSI2C+oxIa4Ux6DXb5R4Q'
# https://github.com/inspircd/inspircd-docker
create_inspircd() {
    "${pun[@]}" rm -rf "${insdata:-/tmp/fake}"
    "${pun[@]}" cp -rL "$rootdir/resources/inspircd" "$insdata"
    "${pun[@]}" chown -R 10000:10000 "$insdata"
    podman create \
        --pod irc \
        --name "net-${1:-foo}" \
        --log-driver json-file \
        --no-hosts \
        --env INSP_ENABLE_DNSBL=no \
        --env "INSP_NET_SUFFIX=.example.net" \
        --env "INSP_NET_NAME=${2:-foonet}" \
        --env "INSP_OPER_PASSWORD_HASH=$inspass" \
        --env "INSP_SERVICES_NAME=services.example.net" \
        --env "INSP_SERVICES_IPADDR=127.0.0.1" \
        --env "INSP_SERVICES_ALLOWMASK=127.0.0.1/32" \
        --env "INSP_SERVICES_PASSWORD=password" \
        --volume "$insdata:/inspircd/conf.d/:Z" \
        "docker.io/inspircd/inspircd-docker:$INSPIRCD_VERSION"
}

# https://github.com/overdrivenetworks/atheme-docker
create_atheme() {
    "${pun[@]}" rm -rf "${athdata:-/tmp/fake}"
    "${pun[@]}" cp -rL "$rootdir/resources/atheme" "$athdata"
    sed -i "s|@PROTOCOL@|${1:-inspircd}|" "$athdata/atheme.conf"
    sed -i "s/@SERVER_NAME@/irc.example.net/" "$athdata/atheme.conf"
    sed -i "s/@HOSTNAME@/services.example.net/" "$athdata/atheme.conf"
    sed -i "s/@PASSWORD@/password/" "$athdata/atheme.conf"
    sed -i "s/@NETWORK@/${2:-foonet}/" "$athdata/atheme.conf"
    sed -i "s/@ADMIN@/oper/" "$athdata/atheme.conf"
    sed -i "s/@PORT@/7000/" "$athdata/atheme.conf"
    "${pun[@]}" chown -R 10000:10000 "$athdata"
    podman create \
        --pod irc \
        --name atheme \
        --log-driver json-file \
        --no-hosts \
        --volume "$athdata:/atheme/etc:Z" \
        "docker.io/ovdnet/atheme:$ATHEME_VERSION"
}

create_fsbot() {
    podman create \
        --pod irc \
        --name fsbot \
        --log-driver json-file \
        --env "ERBOT_NICK=${ERBOT_NICK:-fsbot}" \
        --no-hosts \
        "${ERBOT_IMAGE:-registry.gitlab.com/emacs-erc/erbot/fsbot:latest}" \
        "$@"
}

# bash $this_script run_emacs make lOG_DIR=/project/logs/erc-logs/foo ...
run_emacs() {
    test -n "$EMACS_IMAGE"
    podman run \
    --rm \
    --tty \
    --interactive \
    --pod irc \
    --name emacs \
    --workdir /project \
    --volume "$rootdir:/project:Z" "$EMACS_IMAGE" \
    "$@"
}

"$@"
