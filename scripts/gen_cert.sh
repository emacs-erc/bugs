#!/bin/sh
conf=$(mktemp)
cat > "$conf" <<EOF
[ req ]
utf8                   = yes
default_bits           = 1024
default_keyfile        = $1
distinguished_name     = dn
encrypt_key            = no
prompt                 = no

[ dn ]
C                      = US
O                      = Example
EOF

openssl req -x509 \
    -config "$conf" \
    -days 3650 \
    -newkey rsa \
    -out "$1"

rm "$conf"

