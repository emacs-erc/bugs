#!/bin/bash
set -e

cd /checkout

rm -vf lisp/erc/erc-loaddefs.el lisp/loaddefs.el lisp/loaddefs.elc
find lisp/erc test/lisp/erc -type f \( -name \*.log -o -name \*.elc \) -delete
