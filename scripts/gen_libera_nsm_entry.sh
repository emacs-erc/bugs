#!/bin/sh

# This targets Alpine's ash or other bash-like shells with job control.
# It creates an entry suitable for inclusion in a nsm-settings-file.
#
# For pinning, "fingerprint" means the digest of the cert's SPKI section
# and not that of the entire cert (the x509 man page uses the latter meaning).
#
# If torsocks is installed, you don't need socat:
#
#   $ torsocks openssl s_client -connect $ADDR:6697 -showcerts </dev/null
#
# You can also drop the /dev/null input and chat with libera:
#
#   $ openssl s_client -connect 127.0.0.1:6697 -crlf
#   ...
#   read R BLOCK
#   :palladium.libera.chat NOTICE * :*** Couldn't look up your hostname
#   > CAP LS
#   :palladium.libera.chat CAP * LS :account-notify away-notify ...

set -e

ADDR=libera75jm6of4wxpxt4aynol3xjmbtxgfyjpu34ss4d7r7q2v5zrpyd.onion:6697
: "${TOR_HOST:=127.0.0.1}"
: "${TOR_PORT:=9050}"

# Don't fork; we want this to die upon completion
socat TCP4-LISTEN:6697 SOCKS4A:$TOR_HOST:$ADDR,socksport=$TOR_PORT &

# Grab digest of the cert's SPKI section
fingerprint=$(
    openssl s_client -connect 127.0.0.1:6697 -showcerts </dev/null 2>/dev/null | \
        awk '/^-+BEG/,/^-+END/' | \
        openssl x509 -pubkey | \
        openssl pkey -pubin -outform der | \
        openssl dgst -sha1 -c | cut -f2 -d" "
)

sha_id=$(printf %s "$ADDR" | sha1sum | cut -f1 -d" ")

cat <<EOF
((:id "sha1:$sha_id"
      :fingerprints ("sha1:$fingerprint")
      :host "$ADDR"
      :conditions (:no-host-match :verify-cert)))
EOF
