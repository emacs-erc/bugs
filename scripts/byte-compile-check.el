;;; byte-compile-check.el --- byte-compile-check.el  -*- lexical-binding: t; -*-


;;; Commentary:
;;

(require 'bytecomp)
(require 'pp)

;;; Code:

(defun byte-compile-check ()
  "Byte compile all command line args.
Basically `elisp-flymake--batch-compile-for-flymake'."
  (let ((byte-compile-log-buffer
         (generate-new-buffer " *dummy-byte-compile-log-buffer*"))
        (coding-system-for-read 'utf-8-unix)
        (coding-system-for-write 'utf-8)
        (byte-compile-dest-file-function (function ignore))
        (pp-use-max-width t)
        (pp-max-width 50)
        collected)
    (while command-line-args-left
      (let* ((file (pop command-line-args-left))
             (byte-compile-log-warning-function
              (lambda (string &optional position _ level)
                (pp (car (push (list :file file
                                     :level level
                                     :msg string
                                     :pos position)
                               collected)))
                t)))
        (byte-compile-file file)))
    (kill-buffer byte-compile-log-buffer)
    (kill-emacs (if collected 1 0))))

(provide 'byte-compile-check)

;;; byte-compile-check.el ends here
