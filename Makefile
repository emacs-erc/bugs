
ifdef MAKECMDGOALS
ifneq ($(words $(MAKECMDGOALS)),1)
REST := $(wordlist 2,10,$(MAKECMDGOALS))
HEAD = $(firstword $(subst /, ,$(REST)))
TAIL = $(subst $(HEAD)/,,$(REST))
.PHONY: $(REST)
MAKECMDGOALS := $(word 1,$(MAKECMDGOALS))
endif
endif

.PHONY: pod-up

repro:
	make -C lisp BUG=$(HEAD) SCENARIO=$(TAIL)

irepro:
	make OP=itest repro $(REST)

repro-live:
	make OP=live repro $(REST)

pod-up:
	bash bugs/$(HEAD)/scenarios/$(TAIL)/create_pod.bash
