#!/bin/sh

# Print a JSON "stream" document to stdout, each object containing data for
# building the main index.html web page.
set -e
set -x

test -d public
test -e public/archive/archive-contents
bug=$1
pkg_links=public/$bug/pkg_links.json

test -d "public/$bug"
test -f "$pkg_links"

created_at=$(jq -r 'last(.[] | .patches.createdAt)' < "$pkg_links")
logs_path=$(jq -r 'last(.[] | .logs.downloadPath)' < "$pkg_links")
patches_path=$(jq -r 'last(.[] | .patches.downloadPath)' < "$pkg_links")

elpa_html=$(
    cd public
    find archive -name "erc-$bug"\*.html | sort | tail -n1
)

BUG=$bug LOGS=$logs_path PATCHES=$patches_path ELPA=$elpa_html CREATED=$created_at jq -n '
(env.CREATED | strptime("%Y-%m-%dT%H:%M:%SZ")) as $d | {
    (env.BUG): {
        logs: env.LOGS,
        patches: env.PATCHES,
        elpa: env.ELPA,
        date: $d | strftime("%x"),
        date_full: $d | strftime("%c")
    }
}'
