#!/bin/sh
set -e
# set -x

GQURL=https://gitlab.com/api/graphql
REPO_SLUG=${CI_PROJECT_PATH:-emacs-erc/bugs}

request=$(
    cat <<EOF
query (\$ref: String!, \$after: String!) {
  project(fullPath: "$REPO_SLUG") {
    pipelines(ref: \$ref, status: SUCCESS, after: \$after) {
      nodes {
        id
        path
        jobs {
          nodes {
            name
            id
            finishedAt
          }
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
}
EOF
)

do_one() {
    response=$(
        printf %s "$request" | \
            jq -R --slurp "{query:., variables: {\"ref\": \"$1\", \"after\": \"$2\"}}" | \
            curl "$GQURL" \
                --silent --show-error --fail --location \
                --header "Content-Type: application/json" \
                --request POST --data @-
    )
    printf %s "$response" | jq '.data.project.pipelines
| {
    id: (
          .nodes | .[] | .jobs.nodes | .[]
          | select( .name == "convert:asciicast")
          | .id | split("/") | .[-1]
        ),
    endCursor: .pageInfo.endCursor,
    hasNextPage: .pageInfo.hasNextPage
  }
'
}

do_until() {
    while true ; do
        got=$(do_one "$1")
        id=$(printf %s "$got" | jq -r '.id')
        if [ -n "$id" ]; then
            printf %s "$id"
            return
        fi
        next=$(printf %s "$got" | jq -r '.hasNextPage')
        if [ "$next" != "true" ]; then
            return 1
        fi
        cursor=$(printf %s "$got" | jq -r '.cursor')
        set - "$cursor"
    done
}

"$@"
