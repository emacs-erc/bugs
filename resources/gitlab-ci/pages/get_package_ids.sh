#!/bin/sh
set -e
set -x

GQURL=https://gitlab.com/api/graphql
project_path=$1
package_name=$2
quantity=${3:-3}

# This is like "emacs-erc/bugs" (no leading slash).
test -n "$project_path"

# Create a query to fetch IDs of the three most recent packages. The output
# should resemble:
#
#   {
#     "data": {
#       "project": {
#         "packages": {
#           "nodes": [
#             {
#               "id": "gid://gitlab/Packages::Generic::Package/30190182",
#               "createdAt": "2024-10-15T19:52:15Z"
#             },
#             {
#               "id": "gid://gitlab/Packages::Generic::Package/30150851",
#               "createdAt": "2024-10-15T02:40:14Z"
#             },
#             {
#               "id": "gid://gitlab/Packages::Generic::Package/30114083",
#               "createdAt": "2024-10-14T07:20:35Z"
#             }
#           ],
#           "pageInfo": {
#             "endCursor": "dead..beef",
#             "hasNextPage": true
#           }
#         }
#       }
#     }
#   }

request=$(
    cat <<EOF
query () {
  project(fullPath: "$project_path") {
    packages(
      packageName: "$package_name"
      packageType: GENERIC
      sort: CREATED_DESC
      first: $quantity
    ) {
      nodes {
        id
        createdAt
      }
    }
  }
}
EOF
)

# This function fetches select info from a single package ID. The return value
# is a single deep and narrow object, the last solo item being "nodes", which
# is a list of files:
#
#   {
#     "data": {
#       "package": {
#         "packageFiles": {
#           "nodes": [
#             {
#               "createdAt": "2024-10-14T07:20:40Z",
#               "fileName": "tests.tar.gz",
#               "id": "gid://gitlab/Packages::PackageFile/152581395",
#               "downloadPath": "/emacs-erc/bugs/-/package_files/152581395/download"
#             },
#             {
#               "createdAt": "2024-10-14T07:20:39Z",
#               "fileName": "archive.tar.gz",
#               "id": "gid://gitlab/Packages::PackageFile/152581393",
#               "downloadPath": "/emacs-erc/bugs/-/package_files/152581393/download"
#             },
#             {
#               "createdAt": "2024-10-14T07:20:38Z",
#               "fileName": "patches.tar.gz",
#               "id": "gid://gitlab/Packages::PackageFile/152581390",
#               "downloadPath": "/emacs-erc/bugs/-/package_files/152581390/download"
#             },
#             {
#               "createdAt": "2024-10-14T07:20:36Z",
#               "fileName": "logs.tar.gz",
#               "id": "gid://gitlab/Packages::PackageFile/152581382",
#               "downloadPath": "/emacs-erc/bugs/-/package_files/152581382/download"
#             }
#           ]
#         }
#       }
#     }
#   }

get_link_from_id() {
    (
        cat <<EOF
query {
  package(id: "$1") {
    packageFiles {
      nodes {
        createdAt
        fileName
        id
        downloadPath
      }
    }
  }
}
EOF
    ) | jq -R --slurp '{query:.}' | \
        curl "$GQURL" \
             --silent --show-error --fail --location \
             --header "Content-Type: application/json" \
             --request POST --data @-
}

# This function creates a list of package objects keyed by file stem and
# sorted by date (ascending). Output is a *list* of objects, each resembling:
#
#  {
#    "tests": {
#      "createdAt": "2024-10-15T19:52:18Z",
#      "fileName": "tests.tar.gz",
#      "id": "gid://gitlab/Packages::PackageFile/152928906",
#      "downloadPath": "/emacs-erc/bugs/-/package_files/152928906/download"
#    },
#    "archive": {
#      "createdAt": "2024-10-15T19:52:17Z",
#      "fileName": "archive.tar.gz",
#      "id": "gid://gitlab/Packages::PackageFile/152928904",
#      "downloadPath": "/emacs-erc/bugs/-/package_files/152928904/download"
#    },
#    "patches": {
#      "createdAt": "2024-10-15T19:52:16Z",
#      "fileName": "patches.tar.gz",
#      "id": "gid://gitlab/Packages::PackageFile/152928900",
#      "downloadPath": "/emacs-erc/bugs/-/package_files/152928900/download"
#    },
#    "logs": {
#      "createdAt": "2024-10-15T19:52:15Z",
#      "fileName": "logs.tar.gz",
#      "id": "gid://gitlab/Packages::PackageFile/152928897",
#      "downloadPath": "/emacs-erc/bugs/-/package_files/152928897/download"
#    }
#  }
#
# This also (currently) describes the shape of the final returned stream for
# this script.

transform() {
    jq --slurp '[
      .[].data.package.packageFiles.nodes
        | [ .[] | {key: (.fileName | split(".") | .[0]), value: .} ]
        | from_entries
    ]
      | sort_by(.archive.createdAt | strptime("%Y-%m-%dT%H:%M:%SZ"))'"[-$quantity:]"
}

# This script used to page through till the end, using an 'after' keyword for
# the 'packages' resource. However, that can't be combined with 'first' for
# some reason, so the 'variables' parameter is likely superfluous.
#
# https://docs.gitlab.com/ee/api/graphql/reference/index.html#pagination-arguments

collected=$(mktemp)
just_ids=$(mktemp)

prepped=$(
    printf %s "$request" | jq -R --slurp "{query:., variables: {}}"
)

full_result=$(
    printf %s "$prepped" | curl "$GQURL" \
         --silent --show-error --fail --location \
         --header "Content-Type: application/json" \
         --request POST --data @-
)

printf %s "$full_result" | \
    jq -r '.data.project.packages.nodes | .[] | .id' > "$just_ids"

# Fetch download links and filenames for each ID, and resort.
while read -r id
do get_link_from_id "$id" >> "$collected"
done < "$just_ids"
rm "$just_ids"

transform < "$collected"
rm "$collected"
