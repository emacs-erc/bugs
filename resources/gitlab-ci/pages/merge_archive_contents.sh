#!/bin/sh

# Build Emacs package archive
set -e
set -x

test -d public
test -e public/archive/archive-contents
GITLAB_BASE_URL=https://gitlab.com

# Expect to be called with a list of "bug links" that look like
#
#   public/48598/pkg_links.json ... public/49860/pkg_links.json
#
for pkg_link ; do
    test -f "$pkg_link"
    # This iterates over a list of URL paths, like:
    #
    #  /emacs-erc/bugs/-/package_files/152581393/download
    #
    jq -r '.[].archive.downloadPath' < "$pkg_link" | \
        while read -r archive_path; do
            cd ./public
            # Clober existing archive tar and files with each iteration.
            curl --silent --show-error --fail --location \
                --output archive.tar.gz "${GITLAB_BASE_URL}$archive_path"
            tar -xf archive.tar.gz
            rm -f archive.tar.gz
            # Enter public/archive (merged from all bugs).
            cd archive
            ( echo; cat archive-contents-entry.eld ) >> archive-contents
            test -e index.html || mv index.html.top index.html
            cat ./*.index.html.entry >> index.html
            rm -f ./*.index.html.entry
            rm -f archive-contents-entry.eld
            # Leave public/archive.
            cd ../..
        done
done
