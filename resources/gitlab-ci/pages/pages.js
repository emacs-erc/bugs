/*
@licstart  The following is the entire license notice for the
JavaScript code in this page.

Copyright (C) 2021  J.P. Neverwas

The JavaScript code in this page is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.


@licend  The above is the entire license notice
for the JavaScript code in this page.
*/

// TODO: redo this whole thing proper

{
  const APIEndpoint = 'https://gitlab.com/api/graphql';

  const BaseJobsURL = document.head
        .querySelector('meta[name="base-jobs-url"]').attributes.content.value;

  const ProjectPath = document.head
        .querySelector('meta[name="project-path"]').attributes.content.value;

  // This assumes that last = first and is named convert:asciinema
  const query = `
    query ($ref: String!, $after: String!, $fullPath: ID!) {
      project(fullPath: $fullPath) {
        pipelines(ref: $ref, status: SUCCESS, after: $after) {
          nodes {
            id
            path
            jobs {
              nodes {
                name
                id
                finishedAt
              }
            }
          }
          pageInfo {
            endCursor
            hasNextPage
          }
        }
      }
    }
  `.trim();

  // Attempt to retrieve the Job ID from last job of the last successful
  // pipeline. See pages/get_job_id.sh for a curl/jq example.
  const queryGitLab = async (query) => {
    let request = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify(query)
    };
    let response = await fetch(APIEndpoint, request);
    if (!response.ok) throw new Error(response.statusText);
    return await response.json();
  }

  // Given the shape of our query above, return just the job node
  const pluckJob = (data, ...names) => {
    let pipes = data?.data?.project?.pipelines?.nodes;
    console.assert(pipes?.length, "failed", data, names);
    for (let pipe of pipes) {
      if (!pipe.jobs?.nodes?.length) continue;
      for (let job of pipe.jobs.nodes) {
        if (names.includes(job.name)) return job;
      }
    }
    return null;
  };

  const findJob = async ({branch, names, cursor}) => {
    let data = await queryGitLab(
      {query, variables: {ref: branch, after: (cursor || ''), fullPath: ProjectPath}}
    );
    let job = pluckJob.call(null, data, ...names);
    if (job) return job;
    let info = data?.data?.project?.pipelines?.pageInfo;
    if (!info.hasNextPage) {
      console.error(data);
      throw new Error('Bad query or response');
    }
    return await findJob({branch, names, cursor: info.endCursor});
  }

  // Path starts beneath the job-artifacts logs dir and includes the branch
  const constructURL = (path, job) => {
    let [branch, ...rest] = path.split('/');
    let jobID = job.id.split('::').pop().split('/').pop();
    let pf = rest.length && rest[rest.length - 1].includes('.') ? 'file' : 'browse';
    return `${BaseJobsURL}/${jobID}/artifacts/${pf}/logs/${path}`;
  };

  // When location has a param like ?bug=my-bug, redirect to gitlab browse page
  const maybeRedirect = async () => {
    let params = (new URL(window.location)).searchParams;
    let bug = params.get('bug');
    if (!bug) return;
    let branch = bug.split('/')[0];
    let job = await findJob({branch, names: ['convert:asciicast']});
    window.location.href = constructURL(bug, job);
  };

  // See https://stackoverflow.com/a/7641812 for this relative-date approach
  const mins = 60,
      hour = mins * 60,
      day = hour * 24,
      week = day * 7,
      month = week * 4;
  const rewriteDate = date => {
    let d = Math.round((new Date - date) / 1000);
    return (
      d < 30 ? 'now'
      : d < mins ? `${d} seconds ago`
      : d < 2 * mins ? 'a minute ago'
      : d < hour ? Math.floor(d / mins) + ' minutes ago'
      : Math.floor(d / hour) == 1 ? 'an hour ago'
      : d < 3 * day ? Math.floor(d / hour) + ' hours ago'
      : d < 12 * week ? Math.floor(d / day) + ' days ago'
      : Math.floor(d / month) + ' months ago'
    );
  };

  // Replace 'latest' with date and update links to target latest jobs
  const rewriteItem = async bug => {
    let branch = bug.querySelector('h5').innerText.trim();
    // Rewrite date first
    let job = await findJob(
      {branch, names: ['convert:asciicast', 'package:trunk:' + branch]}
    );
    let dateElt = bug.querySelector('small.date');
    let date = new Date(job.finishedAt);
    dateElt.innerText = rewriteDate(date.valueOf());
    dateElt.setAttribute('title', date.toGMTString());
    // Now iter over any links and rewrite their hrefs
    /*
     * let links = bug.querySelectorAll('a[data-path]');
     * for (let link of links) {
     *   let path = link.attributes['data-path'].value;
     *   let old = link.href;
     *   link.href = constructURL(path, job);
     * }
     */
  };

  const debbugsRequest = (_, bug) => `
<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope
    soapenc:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:ns3="urn:Debbugs/SOAP/TYPES"
    xmlns:ns1="urn:Debbugs/SOAP"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
<soap:Body>
<ns1:get_status>
<ns1:bugs xsi:type="soapenc:Array" soapenc:arrayType="xsd:int[1]">
<ns1:bugs xsi:type="xsd:int">${bug}</ns1:bugs>
</ns1:bugs>
</ns1:get_status>
</soap:Body>
</soap:Envelope>`.trim();

  const debbugsProxyURL = 'https://debbugs.indignant.workers.dev/gnu/';
  const digPat = /^\d+$/;
  const dateItems = ['date', 'last_modified', 'log_modified'];

  const extractBugData = text => {
    let parser = new DOMParser,
        dom = parser.parseFromString(text, "text/xml").documentElement,
        data = dom.querySelector('s-gensym3 > item > value');
    if (!data) throw new Error(`Problem parsing text: ${text}`);
    let bug = {};
    // Guess fetch API doesn't give us an XMLHttpRequest.responseXML; either
    // way, Xpath selection is no fun here. Not sure if it's the namespaced
    // document or what, but ugh.
    for (let node of data.childNodes) {
      if (node.firstChild?.nodeType !== 3) continue;
      if (digPat.exec(node.textContent)) {
        bug[node.nodeName] = dateItems.includes(node.nodeName) ?
          new Date(node.textContent * 1000) : node.textContent * 1;
        continue;
      }
      bug[node.nodeName] = node.textContent;
    }
    return bug;
  };

  const queryDebbugs = async (bug) => {
    let request = {
      method: 'POST',
      headers: {
        'Content-Type': 'text/xml; charset=utf-8',
        'SoapAction': '"Debbugs/SOAP"',
        'Accept': '*/*',
      },
      body: debbugsRequest`${bug}`
    };
    let response = await fetch(debbugsProxyURL, request);
    return await response.text();
  };

  const addCrappyToolTip = async (bug) => {
    let name = bug.id.split('-')[1];
    if (!digPat.exec(name)) return;
    let raw = await queryDebbugs(name),
        bugData = extractBugData(raw);
    bug.querySelector('ul > div').title = [
      `subject: ${bugData.subject}`,
      `originator: ${bugData.originator}`,
      `last modified: ${rewriteDate(bugData.last_modified)}`
    ].join('\n')
  };

  // Main
  (async() => {
    let bugs = document.getElementById("bugitems").children;
    await maybeRedirect();
    for (let bug of bugs) {
      await rewriteItem(bug);
    }
    for (let bug of bugs) {
      await addCrappyToolTip(bug);
    }
  })();
}
