#!/bin/sh

# This deletes all matching files in a package except the latest.
#
# usage: sh ./delete_surplus_files.sh delete [PKG_NAME] [PKG_FILE]

set -e
set -x
# This should look something like:
# https://gitlab.com/api/v4/projects/63061290
test -n "$BASE_API_URL"
test -n "$CI_JOB_TOKEN"

# This query returns a package ID from a list of objects resembling:
# [
#   {
#     "id": 30666896,
#     "name": "master",
#     "version": "0.1",
#     "package_type": "generic",
#     "status": "default",
#     "_links": {
#       "web_path": "/emacs-erc/jabbycat/-/packages/30666896"
#     },
#     "created_at": "2024-10-29T18:10:33.727Z",
#     "last_downloaded_at": "2024-10-30T23:57:13.782Z",
#     "tags": [],
#     "pipeline": {
#     ...
#     },
#     "pipelines": []
#   },
# ...
# ]

# Filter a query like the above to return the most recent ID for a given
# package name.
get_id() {
    test -n "$1" # package name
    _result=$(
        curl --silent --show-error --fail --location \
            "$BASE_API_URL/packages/"
    )
    printf %s "$_result" | \
        jq "[ .[] | select(.name == \"$1\") ]
            | sort_by(.created_at)
            | last
            |.id // empty"
}

# Return an ID of an object matching a version and package name from a query
# result like the above.
get_id_versioned() {
    test -n "$1" # package name
    test -n "$2" # version
    _result=$(
        curl --silent --show-error --fail --location \
            "$BASE_API_URL/packages/"
    )
    printf %s "$_result" | \
        jq ".[]
            | select(.name == \"$1\")
            | select(.version == \"$2\")
            | .id"
}

# Given a package ID, this returns a list of objects like:
# [
#   {
#     "id": 155420962,
#     "package_id": 30666896,
#     "created_at": "2024-10-29T18:33:56.906Z",
#     "file_name": "archive.tar.gz",
#     "size": 4045,
#     ...
#     ]
#   },
#   {
#     "id": 155672072,
#     "package_id": 30666896,
#     "created_at": "2024-10-30T19:13:41.064Z",
#     "file_name": "archive.tar.gz",
#     "size": 3920,
#     ...
#   }
# ]
get_files_by_id() {
    test -n "$1" # package id
    curl --silent --show-error --fail --location \
        "$BASE_API_URL/packages/$1/package_files"
}

# Output all obsolete file IDs.
get_surplus_file_ids() {
    test -n "$1" # package id
    test -n "$2" # file name
    _result=$(get_files_by_id "$1")
    printf %s "$_result" | \
        FILE_NAME=$2 jq '
            [ .[] | select(.file_name == env.FILE_NAME)
            ] | sort_by(.created_at) | .[:-1][] | .id'
}

# For package $1, delete all files matching $2 except the latest.
# Optionally accept a version to filter against as $3.
# https://docs.gitlab.com/ee/api/packages.html
delete() {
    test -n "$1" # package name
    test -n "$2" # file name
    if test -n "$3"; then
        package_id=$(get_id_versioned "$1" "$3")
    else
        package_id=$(get_id "$1")
    fi
    test -n "$package_id"
    file_ids=$(get_surplus_file_ids "$package_id" "$2")
    printf %s "$file_ids" | while read -r file_id; do
        curl --silent --show-error --fail \
        --header "JOB-TOKEN: $CI_JOB_TOKEN" \
        --request DELETE \
        "$BASE_API_URL/packages/$package_id/package_files/$file_id"
    done
}

"$@"
