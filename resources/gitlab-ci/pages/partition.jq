# This is a jq file; Use the -f option to read it in

# Assume input is a list of rel paths like root/bug#/the/rest/../sentinel

# Need awareness of whole topology, so ingest the and split manually
# And slice to drop trailing newline

. / "\n" | .[:-1]

| [
    # For each element, which is a string file path, split into components
    .[] / "/"

    # Lose common root and leaf (our search needle from find(1))
    | .[1:-1]

    | { "branch": .[0], "sub": [.[2:] | join("/")] }
  ]

# This operation retains the outer array, so we gain a level
| group_by(.branch)

# Drop the outer array, and for each newly promoted array,
# iterate over its members, which are the objects we just created
| .[] | [

  # Combine grouped objects
  reduce .[] as $item (
    {branch: "", sub: []};
    {branch: $item.branch, sub: (.sub + $item.sub)}
  )

# Return a stream with branches mapped to objects containing subs
] | .[] | {(.branch): {subs: .sub}}
