#!/bin/sh

# Given a ref as $1, print the JOB_ID of the last job from the most recent
# successful pipeline

TARGET_JOB=convert:asciicast

set -e
test -n "$1"

GQURL=https://gitlab.com/api/graphql
REPO_SLUG=${CI_PROJECT_PATH:-emacs-erc/bugs}

payload="
query (\$ref: String!, \$after: String!) {
  project(fullPath: \"$REPO_SLUG\") {
    pipelines(ref: \$ref, status: SUCCESS, after: \$after) {
      nodes {
        id
        path
        job (name: \"$TARGET_JOB\") {
            id
            finishedAt
            artifacts {
              nodes {
                name
                downloadPath
              }
            }
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
}
"

query='{query:., variables: {ref: env.REF, after: env.AFTER}}' 

get_one() {
    printf %s "$payload" | REF=$1 AFTER=$2 jq -R --slurp "$query" | \
    curl "$GQURL" \
        --silent --show-error --fail --location \
        --header "Content-Type: application/json" \
        --request POST --data @-
}

nodes=$(mktemp)
truncate -s0 "$nodes"
_temp=$(mktemp)
while :; do
    get_one "$@" > "$_temp"
    jq '.data.project.pipelines.nodes | .[] | select(.job)' < "$_temp" >> "$nodes"
    if [ "$(jq '.data.project.pipelines.pageInfo.hasNextPage' < "$_temp")" = true ]; then
        cursor=$(jq -r '.data.project.pipelines.pageInfo.endCursor' < "$_temp")
        set - "$1" "$cursor"
        continue
    fi
    break
done

REF=$1 jq -s '
[ .[] | .job | select( (.artifacts.nodes | length) == 3 ) ][0]
| (.finishedAt | strptime("%Y-%m-%dT%H:%M:%SZ")) as $d
| {
    (env.REF): {
        "job_id": (.id | split("/") | .[-1]),
        "date": $d | strftime("%x"),
        "date_full": $d | strftime("%c")
    }
}' < "$nodes"
