FROM docker.io/library/golang:alpine AS build

ADD . /go/src/git.sr.ht/~emersion/soju
WORKDIR /go/src/git.sr.ht/~emersion/soju

RUN apk add --update gcc musl-dev
RUN CGO_ENABLED=1 go get -t -v ./... && go build -a ./...

FROM alpine:latest
COPY --from=build /go/bin/* /bin/
# COPY --from=build /etc/ssl/* /etc/ssl/
# COPY --from=build /etc/ssl1.1/* /etc/ssl1.1/

ADD config.in /etc/soju/config
RUN mkdir /var/lib/soju

CMD ["/bin/soju"]
# vim:ft=dockerfile
