# https://github.com/DanielOaks/irc-parser-tests
# Rev: a1c8e14fb6699b3d34a14cc7672d6f34c917e412
import os
import yaml
import json
from pathlib import Path

WANT_SEPARATE = os.getenv("WANT_SEPARATE", False) and True
data = {}

for p in Path("tests").iterdir():
    d = yaml.safe_load(p.read_text())
    data[p.stem] = d

    if WANT_SEPARATE:
        with p.with_suffix(".json").open("w") as flow:
            json.dump(d, flow)

with open("all.json", "w") as flow:
    json.dump(data, flow)
