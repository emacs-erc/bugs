;; See: https://github.com/DanielOaks/irc-parser-tests
;; Test cases are yaml documents under CC0
(require 'cl-extra)
(require 'json)
(let ((all (with-temp-buffer
             (insert-file-contents-literally "all.json")
             (json-parse-buffer :object-type 'alist :array-type 'list))))
  (with-temp-buffer
    (add-file-local-variable-prop-line 'mode 'lisp-data)
    (goto-char (point-max))
    (insert "\n;; https://github.com/DanielOaks/irc-parser-tests\n")
    (princ (buffer-string) standard-output))
  (pp all))
