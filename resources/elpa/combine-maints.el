;; -*- lexical-binding: t; -*-
(require 'package)

(defvar in-place (getenv "COMBINE_MAINTS_INPLACE"))

(defun join (maints)
  (let* ((rest (reverse maints))
         (trailing (pop rest)))
    (cons (mapconcat (lambda (maint)
                       (if (cdr maint)
                           (concat (car maint) " <" (cdr maint) ">")
                         (car maint)))
                     (nreverse (cons (list (pop trailing)) rest))
                     ", ")
          trailing)))

;; erc-pkg.el
(defun do-pkg-el ()
  (let ((f (pop command-line-args-left))
        (backup-inhibited t))
    (with-temp-buffer
      (insert-file-contents (expand-file-name f))
      (lisp-data-mode)
      (let* ((sexp (read (current-buffer)))
             (m (plist-get (cdr sexp) :maintainer))
             (updated (join (cadr m))))
        (setf (plist-get (cdr sexp) :maintainer) (list 'quote updated))
        (delete-region (line-beginning-position) (point-max))
        (prin1 sexp (current-buffer)))
      (if in-place
          (write-file f nil)
        (message "%s" (buffer-string))))))


;; archive-contents (singleton)
(defun do-ac ()
  (let ((f (pop command-line-args-left))
        (backup-inhibited t))
    (with-temp-buffer
      (insert-file-contents (expand-file-name f))
      (lisp-data-mode)
      (let* ((sexp (read (current-buffer)))
             (e (package--ac-desc-extras (cdadr sexp)))
             (m (alist-get :maintainer e))
             (updated (join m)))
        (setf (alist-get :maintainer e) updated)
        (delete-region (point-min) (point-max))
        (pp sexp (and in-place (current-buffer))))
      (when in-place
        (write-file f nil)))))
