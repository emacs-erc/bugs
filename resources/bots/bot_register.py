import asyncio
import codecs
import random
import os
import stat
import irc3

from irc3.plugins import human
from irc3.utils import IrcString


@irc3.plugin
class MyHuman(human.Human):

    def __init__(self, bot):
        super().__init__(bot)
        self.delay = (3, 7)

    def _get_line(self):
        pos = random.randint(0, os.stat(self.db)[stat.ST_SIZE])
        with codecs.open(self.db, encoding=self.bot.encoding) as fd:
            fd.seek(pos)
            fd.readline()
            try:
                return fd.readline().strip()
            except Exception:  # pragma: no cover
                pass

    def _call_with_double_delay(self, func, *args, **kwargs):
        orig = self.delay
        self.delay = (orig[0], orig[1]*2)
        try:
            self.call_with_human_delay(func, *args, **kwargs)
        finally:
            self.delay = orig

    @irc3.event(irc3.rfc.MY_PRIVMSG)
    def on_message(self, mask=None, event=None, target=None, data=None, **kw):
        if not target.is_channel:
            return
        plain = None
        message = '{0}: {1}'.format(mask.nick, self._get_line() or 'Yo!')
        if round(random.random()):
            plain = self._get_line()
            if plain:
                self.call_with_human_delay(self.bot.privmsg, target, plain)
                self._call_with_double_delay(self.bot.privmsg, target, message)
                return
        self.call_with_human_delay(self.bot.privmsg, target, message)

    @irc3.event(irc3.rfc.PRIVMSG)
    def on_msg(self, mask, target=None, data=None, **kw):
        if target.is_channel:
            return
        if mask.nick.lower() == "nickserv":
            return
        target = IrcString(mask.nick)
        message =  self._get_line()
        if message:
            self.call_with_human_delay(self.bot.privmsg, target, message)

@irc3.plugin
class Plugin:
    requires = ["irc3.plugins.core", MyHuman]

    def __init__(self, bot):
        self.bot = bot

    @irc3.event(irc3.rfc.JOIN)
    def on_join(self, mask, channel, **kw):
        """Say hi whenever someone joins"""
        if mask.nick != self.bot.nick:
            self.bot.privmsg(channel, "%s, welcome!" % mask.nick)
            return
        self.bot.channel = channel
        self.bot.privmsg(channel, "Hi!")

    @irc3.event(irc3.rfc.PRIVMSG)
    def on_msg(self, mask, data=None, **kw):
        """Log anything said"""
        self.bot.log.info("%s: %s", mask.nick, data)

    async def _register(self, password, email):
        delay = self.bot.config.delay
        while delay:
            self.bot.log.info("Sleeping for %r more second(s)" % delay)
            await asyncio.sleep(1)
            delay -= 1
        self.bot.privmsg("NickServ", "REGISTER %s %s" % (password, email))

    def _on_motd_end(self, **kw):
        """Register with NickServ"""
        password = self.get_password(self.bot.nick)
        email = "%s@example.net" % kw.get("me", self.bot.nick)
        if not password:
            return
        asyncio.create_task(self._register(password, email))

    @irc3.event(irc3.rfc.ERR_NOMOTD)
    def on_nomotd(self, **kw):
        return self._on_motd_end(**kw)

    @irc3.event(irc3.rfc.RPL_ENDOFMOTD)
    def on_motd_end(self, **kw):
        return self._on_motd_end(**kw)

    @irc3.event(r'(@(?P<tags>\S+) )?:(?P<ns>NickServ)!NickServ@services.'
                r' NOTICE (?P<nick>irc3) :This nickname is registered.*')
    def register(self, ns=None, nick=None, **kw):
        assert nick
        password = self.get_password(nick)
        if not password:
            self.bot.log.error("Password was None")
            return
        self.bot.privmsg(ns, 'IDENTIFY %s %s' % (nick, password))

    def get_password(self, nick):
        host = self.bot.config.host
        try:
            password = self.bot.config[host][nick]
        except KeyError:
            self.bot.log.error("Couldn't find password for %s" % host)
            return "changeme"
        return password
