;;; repro-i.el --- Helpers for inferior repro recipe -*- lexical-binding: t; -*-

;;; Commentary:

(require 'repro-common)
(require 'trace)

;;; Code:

(defvar repro-i-max-wait-inner 100)

(declare-function erc-toggle-debug-irc-protocol "erc" (&optional arg))

(defun repro-i-log (&rest args)
  "Log silently to messages buffer with time stamp.
ARGS are passed to `format'."
  (let ((inhibit-message t))
    (when (= (length args) 1)
      (setq args (cons "%s" args)))
    (message "[%s] %s" (format-time-string "%F %T.%6N") (apply 'format args))))

;; When called using the &rest form, `minibuffer-message' always seems to
;; supply the last message intact (without gaps) However, sometimes an extra
;; newline is thrown in, which hikes the message up a line and is quite
;; distracting.  The following is a lazy attempt at balancing the flickering
;; caused by clearing (to remove skipped chars) while minimizing the mentioned
;; undesirable effects.  Goes without saying we need a better understanding of
;; how minibuffer.el works.
(defun repro-i-speak--minibuffer (msg timeout clear)
  "Display minibuffer MSG for TIMEOUT secs.
With CLEAR, remove existing message beforehand."
  (let (message-log-max
        (minibuffer-message-timeout timeout))
    (when clear (message nil))
    (minibuffer-message "%s" msg)))

(defun repro-i-speak (msg &rest rest)
  "Say MSG, maybe applying formatting with args REST.
MSG can also be a number N, in which case the car of REST becomes MSG,
which will last for N seconds."
  (let (timeout)
    (when (numberp msg)
      (setq timeout msg
            msg (pop rest)))
    (repro-i-log msg)
    (when rest
      (setq msg (apply #'format msg rest)))
    (setq msg (propertize (concat ";; " msg) 'face 'font-lock-comment-face))
    (let ((delay 0)
          (len (length msg))
          (n 0))
      (while (< n len)
        (run-at-time (cl-incf delay 0.018) nil #'repro-i-speak--minibuffer
                     (substring msg 0 (cl-incf n)) timeout
                     (or (= n len) (zerop (% n 5))))))))

;; It would be nice to somehow know whether we're in trace-entry-message or
;; trace-exit-message.

;; FIXME only print what we need.  This seems too wasteful.
(defun repro-i--dump-locals ()
  "Print all buffer local vars."
  (format "\n[locals %S BEG]\n%s[locals %S END]\n"
          (current-buffer)
          (pp-to-string (buffer-local-variables))
          (current-buffer)))

(defun repro-i--dump-buffer-list ()
  "Print a list of buffers."
  (format "\n[buffers BEG]\n%S\n[buffers END]\n" (buffer-list)))

(defun repro-i--dump-process-list ()
  "Print a list of procs."
  (format "\n[processes BEG]\n%S\n[processes END]\n"
          (mapcar (lambda (p) (cons p (process-status p))) (process-list))))

(defun repro-i--dump-common ()
  "Print some basic context info."
  (concat (format-time-string "\n[%F %T.%6N]\n")
          (repro-i--dump-buffer-list)
          (repro-i--dump-process-list)
          (repro-i--dump-locals)))

(defun repro-i-instrument-common ()
  "Install trace hooks for inferior recipe."
  (trace-function-background 'erc-generate-new-buffer-name
                             nil #'repro-i--dump-common)
  (trace-function-background 'erc-server-connect
                             nil #'repro-i--dump-common)
  (trace-function-background 'erc-setup-buffer
                             nil #'repro-i--dump-common)
  (trace-function-background 'erc-server-JOIN
                             nil #'repro-i--dump-common))

(defun repro-i--get-log-file-name ()
  "Generate a user-friendly name for log file based on current buffer."
  ;; Makefiles don't like colons, shells don't like *#
  (let ((name (buffer-name)))
    (dolist (subs '(("[*#>]" . "") ("[.]" . "_") (":" . "p") ("[</]" . "-")))
      (setq name (replace-regexp-in-string (car subs) (cdr subs) name)))
    (concat "buf-" name ".log")))

(defun repro-i--dump-buffers ()
  "Save inferior repro logs to file."
  (dolist (buf (buffer-list))
    (with-current-buffer buf
      (when (or (memq major-mode '(messages-buffer-mode
                                   ibuffer-mode
                                   erc-mode
                                   debugger-mode))
                (member (buffer-name)
                        '("*trace-output*" "*erc-protocol*")))
        (let ((contents (buffer-string)))
          (with-temp-file (expand-file-name (repro-i--get-log-file-name)
                                            repro-log-dir)
            (insert contents)))))))

(defvar repro-i--recipe-buffer nil)
(defvar repro-i--setup-done nil)
(defvar repro-i--teardown-done nil)

(defun repro-i-setup ()
  "Run some setup tasks for inferior repro recipe."
  (auto-save-mode -1)
  (repro-i-log "repro-bug-recipe: %s" repro-bug-recipe)
  (repro-i-log "emacs-version: %s" emacs-version)
  (repro-i-log "emacs-repository-branch %s" emacs-repository-branch)
  (repro-i-log "emacs-repository-version %s" emacs-repository-version)
  (repro-i-instrument-common)
  (with-current-buffer (find-file-noselect repro-bug-recipe)
    (set-variable 'erc-server-auto-reconnect nil)
    (require 'erc)
    (erc-toggle-debug-irc-protocol)
    (setq repro-i--recipe-buffer (current-buffer))
    (pop-to-buffer-same-window repro-i--recipe-buffer)
    (sit-for 0.1))
  (setq repro-i--setup-done t))

(defun repro-i-teardown ()
  "Run some teardown tasks for inferior repro script."
  (unless repro-i--teardown-done
    (setq repro-i--teardown-done t)
    (repro-i--dump-buffers)
    (dolist (proc (process-list))
      (set-process-query-on-exit-flag proc nil)
      (delete-process proc))))

(defun repro-i-execute ()
  "Execute inferior repro script with timeout."
  (cl-assert repro-i--setup-done)
  (cl-assert (buffer-live-p repro-i--recipe-buffer))
  (let (debug-on-error)
    (with-timeout (repro-i-max-wait-inner
                   (repro-i-teardown)
                   (error "Timed out awaiting inferior process"))
      (with-current-buffer repro-i--recipe-buffer
        (eval-buffer)))))

(defun repro-run-inferior ()
  "Run inferior repro script interactively."
  (repro-run--set-vars '("--max-wait" . repro-i-max-wait-inner)
                       '("--debug" . debug-on-error))
  (when (stringp debug-on-error)
    (setq debug-on-error t))
  (when (stringp repro-i-max-wait-inner)
    (setq repro-i-max-wait-inner (string-to-number repro-i-max-wait-inner)))
  (repro-i-setup)
  (redisplay)
  (repro-i-execute)
  ;; XXX Read "live" timeout somehow from scenario dir; for now just hard code
  (run-at-time (setq repro-i-max-wait-inner 60) nil #'repro-i-teardown))

(provide 'repro-i)

;;; repro-i.el ends here
