;;; repro-common.el --- Helpers for repro -*- lexical-binding: t; -*-


;;; Commentary:

;;; Code:

(defvar repro-log-dir nil)
(defvar repro-bug-recipe nil)

(defun repro-run--set-vars (&rest options)
  "Extract vars from command line args for repro batch commands.
For each (FLAG . SYM) in OPTIONS, set SYM to the value following FLAG
if present."
  (when options
    (let ((args command-line-args-left)
          arg rest)
      (while (setq arg (pop args))
        (if-let ((sym (and (string-prefix-p "-" arg)
                           (assoc-default arg options))))
            (set sym (pop args))
          (push arg rest)))
      (setq command-line-args-left (reverse rest))))
  (setq repro-bug-recipe (pop command-line-args-left)
        repro-log-dir (pop command-line-args-left))
  (unless (and (file-exists-p repro-log-dir)
               (file-exists-p repro-bug-recipe))
    (error "Bad args"))
  (or (pop command-line-args-left) 'repro-test))

(provide 'repro-common)

;;; repro-common.el ends here
