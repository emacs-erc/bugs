;;; repro.el --- Some helpers for reproducing ERC bugs -*- lexical-binding: t; -*-

;;; Commentary:

;; This is a half-baked mini framework for running minimal bug "recipes" in a
;; term.el subprocess.  A superior companion process runs in tandem and
;; asserts various facts and determines the overall success/failure.  The
;; heavy lifting and dirty details can be apportioned according to the needs
;; of the specific scenario under test.  For example, the bulk of the window
;; and display-buffer switching can be driven by the recipe itself, or it can
;; be handled by the superior process, which would act more or less like a
;; camera.  Alternatively, the superior can assume the role of a pilot or
;; driver and, in the tradition of TCL Expect, provide input and assess the
;; results.
;;
;; TODO splain mo better

;;; Code:

(require 'repro-common)
(require 'ert)
(require 'term)
(require 'inline)
(require 'subr-x)
(require 'trace)

(defvar repro-max-wait-outer 120)
(defconst repro--this-file load-file-name)
(defconst repro--this-dir
  (file-name-as-directory (file-name-directory repro--this-file)))

(defconst repro--inferior-process-name "recipe-case")

(defvar repro-want-fail (and (getenv "WANT_FAIL") t))
(defvar repro-want-kill t)

(defvar repro--emacs-exe
  (or (getenv "EMACS")
      (expand-file-name invocation-name invocation-directory)))

;; Assumes a great deal
(defvar repro-mode-line-regexp "^[=U-]\\{,4\\}:[%* -]\\{4\\} F1[[:space:]]+")

;; These repro-term-* functions are helpers to navigate the inferior process.
;; Normally, `process-mark' is char-mode point (where the cursor is).

(defvar term-height nil)
(defmacro repro-term--with-echo-area (&rest body)
  "Run BODY in a term-mode echo area."
  `(save-excursion
     (save-restriction
       (widen)
       (goto-char (term-process-mark))
       (forward-line (- term-height term-current-row 2))
       (when (looking-at-p repro-mode-line-regexp)
         (forward-line 1))
       (progn ,@body))))

(defmacro repro-term-echo-area ()
  "Return the content of the echo area."
  `(repro-term--with-echo-area
    (buffer-substring-no-properties (line-beginning-position)
                                    (line-end-position))))

(defun repro-term--cursor-in-echo-area-p (proc)
  "Return the start of the echo area of term PROC."
  (<= (repro-term--with-echo-area (point)) (process-mark proc) (point-max)))

(defun repro-term-await-message (pattern timeout)
  "Wait for PATTERN to match output in echo area within TIMEOUT secs."
  (with-timeout (timeout (error "Timed out awaiting msg %S" pattern))
    (while (not (string-match-p pattern (repro-term-echo-area)))
      (sleep-for 0.0001))))

(define-inline repro-term-content-p (pattern)
  "Return non-nil if PATTERN is found somewhere on screen."
  (inline-letevals (pattern)
    (inline-quote (save-excursion
                    (save-restriction
                      (widen)
                      (goto-char (term-process-mark))
                      (forward-line (- term-current-row))
                      (let (case-fold-search)
                        (search-forward-regexp ,pattern nil t)))))))

(defun repro-term-await-content (pattern timeout)
  "Wait for PATTERN to match output somewhere on the screen.
If TIMEOUT is exceeded, raise an error."
  (with-timeout (timeout (error "Timed out awaiting pattern %S" pattern))
    (while (not (repro-term-content-p pattern))
      (sleep-for 0.01))))

(defun repro-term-await-content-invert (pattern timeout)
  "Ensure PATTERN does not match anything on screen for TIMEOUT secs."
  (let ((finished (time-add timeout (current-time))))
    (while (time-less-p (current-time) finished)
      (when (save-excursion
              (save-restriction
                (widen)
                (goto-char (term-process-mark))
                (forward-line (- term-current-row))
                (search-forward-regexp pattern nil t)))
        (error "Timed out awaiting sustained absence of pattern %S" pattern))
      (sleep-for 0.01))))

(defun repro-term-await-buffer (name timeout)
  "Wait for NAME to match visible buffer before TIMEOUT secs.
This is often something like `mode-line-buffer-identification' and not
the actual buffer name."
  (repro-term-await-content (concat repro-mode-line-regexp (regexp-quote name))
                            timeout))

(defun repro-superior-dump-line (msg)
  "Print MSG, then whatever's on the current line."
  (message "[%s]: %S" msg (buffer-substring-no-properties
                           (line-beginning-position)
                           (line-end-position))))

(defun repro-superior-dump-window (msg)
  "Print MSG, then whatever's on the terminal screen."
  (message "[%s]:\n%S\n" msg (buffer-substring-no-properties (point-min)
                                                             (point-max))))

(defun repro-term-send (string)
  "Send STRING to terminal.
Use this instead of `execute-kbd-macro'."
  (term-send-string (get-buffer-process (current-buffer)) string))

(defmacro repro-term-eval (form &optional no-result)
  "Send sexp FORM to inferior process' eval prompt and execute.
Return contents of echo area.  With NO-RESULT, return after issuing
command.  FORM is evaluated (possibly twice), so quote it if necessary."
  `(let ((proc (get-buffer-process (current-buffer)))
         latest)
     (with-timeout (1 (error "Cursor clash"))
       (while (repro-term--cursor-in-echo-area-p proc)
         (sleep-for 0.01)))
     (save-excursion
       (goto-char (process-mark proc))
       (term-send-raw-string "\e:"))
     (repro-term-await-message "^Eval: " 2)
     (term-send-raw-string (format "%S\n" ,form))
     (unless ,no-result
       (with-timeout (3 (error "Timed out awaiting result for %S" ,form))
         (while (string-match-p "^Eval: " (setq latest (repro-term-echo-area)))
           (sleep-for 0.01)))
       latest)))

(defun repro-term-speak (timeout &rest msg-args)
  "Say MSG-ARGS and sleep for TIMEOUT."
  (repro-term-eval (cons 'repro-i-speak msg-args))
  (sleep-for timeout))

(defun repro-term-ibuffer (&rest args)
  "Call ibuffer ARGS in term subproc with."
  (repro-term-eval
   `(progn (setq ibuffer-formats '((mark modified read-only locked " "
                                         (name 10 -1 :left :elide) " "
                                         (mode 10 10 :left :elide) " "
                                         filename-and-process)
                                   (mark " " (name 16 -1) " " filename)))
           (ignore ibuffer-formats)
           (ibuffer ,@args))))

(defun repro-term-list-processes ()
  "Call `list-processes' in child Emacs with wider first column."
  (repro-term-eval '(progn (list-processes)
                           (with-current-buffer "*Process List*"
                             (setcdr (aref tabulated-list-format 0) '(30 t))
                             (funcall #'revert-buffer)))))

(defun repro-term-sleep-n-send (&rest rest)
  "Send messages, pausing in between.
REST should be an alternating list of strings and numbers."
  (let ((proc (get-buffer-process (current-buffer)))
        o)
    (while (setq o (pop rest))
      (if (numberp o)
          (sleep-for o)
        (term-send-string proc o)))))

(defun repro-clean-up-inferior (proc)
  "Run some emergency teardown tasks for PROC.
The point here is to salvage the replay file."
  (when-let ((buf (get-buffer (concat "*" repro--inferior-process-name "*"))))
    (with-current-buffer buf
      (term-send-raw-string "\C-g\C-g\C-g")
      (sit-for 0.1)
      (term-send-raw-string "\e:")
      (sit-for 0.1)
      (term-send-string proc
                        (concat "(progn (set-buffer-modified-p nil)"
                                "(with-demoted-errors (repro-i-teardown)))\n"))
      (sit-for 0.1)
      (term-send-raw-string "\C-x\C-c")
      (sit-for 0.1))
    (let ((max-tries 10))
      (while (and (not (zerop (cl-decf max-tries)))
                  (process-live-p proc))
        (sleep-for 0.2))))
  (signal-process proc 'SIGUSR1)
  (set-process-query-on-exit-flag proc nil)
  (delete-process proc))

(defun repro--start-inferior-process ()
  "Assemble term session command line and return the buffer."
  (add-hook 'term-mode-hook (lambda () (term-reset-size 24 80)))
  (let* ((script-file (expand-file-name "replay.script" repro-log-dir))
         (timing-file (expand-file-name "replay.timing" repro-log-dir))
         (args (list repro--emacs-exe "-Q" "-nw"
                     "-L" repro--this-dir "-l" repro--this-file))
         (arg-string (string-join (mapcar #'shell-quote-argument args) " "))
         (buf (make-term repro--inferior-process-name
                         "script" nil "-c" arg-string
                         (concat "--timing=" timing-file) script-file)))
    buf))

(defun repro-execute-bug-recipe ()
  "Run the recipe in the inferior process."
  (ert-info ("Run user steps")
    (repro-term-eval `(repro-i-execute) 'no-result)))

(defmacro repro-with-inferior (&rest body)
  "Start terminal and run BODY.
It's up to the invoking ERT test to call `repro-execute-bug-recipe' at
some point, which will eval the bug steps in `repro-bug-recipe'.
Otherwise this whole thing is a sham."
  (let ((buf (make-symbol "buf"))
        (proc (make-symbol "proc")))
    `(let* ((,buf (repro--start-inferior-process))
            (,proc (get-buffer-process ,buf)))
       (should (string= (process-name ,proc) repro--inferior-process-name))
       (with-current-buffer ,buf
         (sleep-for 0.1)
         (should (process-live-p ,proc))
         (with-timeout (5 (error "Timed out awaiting startup"))
           (while (not (progn (goto-char (point-min))
                              (search-forward "This buffer is for" nil t)))
             (sleep-for 0.1)))
         (term-char-mode)
         (goto-char (process-mark ,proc))
         (sleep-for 0.1)
         (ert-info ("Run user prep")
           (repro-term-eval `(progn (setq repro-log-dir ,repro-log-dir
                                          repro-bug-recipe ,repro-bug-recipe)
                                    (auto-save-mode -1)
                                    (require 'repro-i)
                                    (repro-i-setup))
                            'no-result))
         (sleep-for 0.1)
         (with-timeout (repro-max-wait-outer
                        (error "Timed out awaiting superior process"))
           ,@body)
         (repro-term-eval '(repro-i-teardown) 'no-result)
         (repro-term-eval '(kill-emacs 0) 'no-result))
       (while (process-live-p ,proc)
         (sleep-for 0.1))
       (when (process-live-p ,proc)
         ;; Teardown in subprocess must have failed
         (repro-clean-up-inferior ,proc)
         (error "Failed: process did not exit"))
       ;; We dropped the --return flag, so this should always pass
       (should (zerop (process-exit-status ,proc))))))

(defun repro-run-batch ()
  "Run bug-reproduction simulation non-interactively.
Required args are STEPS-FILE LOG-DIR.  This is basically
`ert-run-tests-batch-and-exit' with some corner-case escape hatches."
  (unless noninteractive (error "Not a batch session"))
  (let ((selector (repro-run--set-vars))
        exit-ok)
    (setq attempt-stack-overflow-recovery nil
          attempt-orderly-shutdown-on-fatal-signal nil)
    (unwind-protect
        (let ((stats (ert-run-tests-batch selector)))
          (unless (setq exit-ok (zerop (ert-stats-completed-unexpected stats)))
            (when-let ((proc (get-process repro--inferior-process-name)))
              (if (process-live-p proc)
                  (repro-clean-up-inferior proc)
                (message "failed"))))
          ;; XOR (test-result, want-inverted) ~~> exit
          ;;      success      true               bad
          ;;      success      false              good
          ;;      failure      false              bad
          ;;      failure      true               good
          (when repro-want-kill
            (kill-emacs (if (eq exit-ok repro-want-fail) 1 0))))
      (unwind-protect
          (progn
            (message "Error running tests")
            (backtrace))
        (when-let ((proc (get-process repro--inferior-process-name)))
          (when (process-live-p proc)
            (repro-clean-up-inferior proc)))
        (when repro-want-kill
          (kill-emacs (if repro-want-fail 0 2)))))))

(defun repro-run-debug ()
  "Run repro test cases interactively.
The `repro-want-fail' doesn't work here."
  (when noninteractive (error "Emacs session not interactive"))
  (let ((selector (repro-run--set-vars)))
    (setq ert-debug-on-error t)
    (ert-run-tests-interactively selector)))

(provide 'repro)

;;; repro.el ends here
