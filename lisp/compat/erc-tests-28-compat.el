;;; erc-tests-28-compat.el --- Emacs 28 backports -*- lexical-binding: t -*-

;;; Commentary:

;; Functions stolen from Emacs 29 for running ERC tests in 28

;;; Code:

(provide 'erc-tests-28-compat)
;;; erc-tests-28-compat.el ends here
