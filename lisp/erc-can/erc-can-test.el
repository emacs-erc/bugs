;;; erc-can-test.el --- Test for cannery  -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(require 'erc-can)
(require 'ert)
(load (expand-file-name "erc-d-tests.el"
                        (if (file-exists-p "erc-d/") "erc-d/" "../erc-d/")))


(defvar erc-can-test-directory (file-name-directory load-file-name))

(ert-deftest erc-can--protocol-regexp-in ()
  (let ((s "\
2021-06-13T03:10:06.551612-0700 chester/foonet >> PASS password123\r"))
    (should (string-match erc-can--protocol-regexp-in s))
    (should (equal (match-string 1 s) "2021-06-13T03:10:06.551612-0700"))
    (should (equal (match-string 2 s) "chester/foonet"))
    (should (equal (match-string 3 s) "PASS password123"))
    (should (equal (match-string 4 s) "PASS"))))


(ert-deftest erc-can--protocol-regexp-out-auth () ; regression
  (let ((s "2021-06-13T03:10:06.551612-0700 foonet << AUTHENTICATE +\r"))
    (should (string-match erc-can--protocol-regexp-out s))
    (should (equal (match-string 1 s) "2021-06-13T03:10:06.551612-0700"))
    (should (equal (match-string 2 s) "foonet"))
    (should (equal (match-string 3 s) "AUTHENTICATE +"))))

(ert-deftest erc-can--protocol-regexp-out ()
  (let ((s "\
2021-06-13T03:10:06.557470-0700 chester/foonet << \
:irc.foonet.org 001 chester :Welcome to the foonet IRC Network chester\r"))
    (should (string-match erc-can--protocol-regexp-out s))
    (should (equal (match-string 1 s) "2021-06-13T03:10:06.557470-0700"))
    (should (equal (match-string 2 s) "chester/foonet"))
    (should (equal (match-string 3 s) "\
:irc.foonet.org 001 chester :Welcome to the foonet IRC Network chester"))
    (should (equal (match-string 4 s) nil))))

(ert-deftest erc-can-convert ()
  (let ((in (expand-file-name "test-data/proto-ts-in.log"
                              erc-can-test-directory))
        (out-tester (expand-file-name "test-data/proto-ts-out-tester.eld"
                                      erc-can-test-directory))
        (out-chester (expand-file-name "test-data/proto-ts-out-chester.eld"
                                       erc-can-test-directory))
        expected
        result)
    (erc-can-convert-protocol-log in)

    (setq result (with-current-buffer "*erc-can-read-tester-foonet*"
                   (buffer-string))
          expected (with-temp-buffer (insert-file-contents out-tester)
                                     (buffer-string)))
    (ert-info ("Tester as expected")
      (should (equal result expected)))

    (setq result (with-current-buffer "*erc-can-read-chester-foonet*"
                   (buffer-string))
          expected (with-temp-buffer (insert-file-contents out-chester)
                                     (buffer-string)))
    (ert-info ("Chester as expected")
      (should (equal result expected)))))

(provide 'erc-can-test)

;;; erc-can-test.el ends here
