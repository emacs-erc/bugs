From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Mon, 9 Sep 2024 15:19:59 -0700
Subject: [PATCH 12/37] [5.x] Add divider variant for
 erc-keep-place-indicator-style

FIXME Add new news item, something like:

;; ** A new 'keep-place-indicator' style emphasizing separation.
;; In response to criticism over the indicator's default appearance, this
;; module now offers a more modern, '<br>'-like separator meant to clearly
;; delineate old messages from unseen ones at a glance.  Set the option
;; 'erc-keep-place-indicator-style' to 'divider' to check it out.

;; * etc/ERC-NEWS: Announce new `divider' style for
;; `erc-keep-place-indicator-style'.
* lisp/erc/erc-goodies.el (erc-keep-place-indicator-style):
New `divider' choice variant.
(erc-keep-place-indicator-divider): New variable and face of the same
name.
(erc-keep-place-indicator-mode, erc-enable-keep-place-indicator): Create
`divider' style overlay.
(erc--keep-place-indicator-create): New function to create overlay.
(erc-keep-place-move): Decorate preceding newline when overlay style is
`heading'.
---
 lisp/erc/erc-goodies.el | 91 ++++++++++++++++++++++++++++++-----------
 1 file changed, 67 insertions(+), 24 deletions(-)

diff --git a/lisp/erc/erc-goodies.el b/lisp/erc/erc-goodies.el
index 133d2943c84..bd2d84dd406 100644
--- a/lisp/erc/erc-goodies.el
+++ b/lisp/erc/erc-goodies.el
@@ -280,11 +280,16 @@ erc-keep-place-indicator-style
 For use with the `keep-place-indicator' module.  A value of `arrow'
 displays an arrow in the left fringe or margin.  When it's
 `face', ERC adds the face `erc-keep-place-indicator-line' to the
-appropriate line.  A value of t does both."
+appropriate line.  A value of t does both.  A value of `divider' shows
+a standalone \"separator\" on its own line in addition to an arrow in
+the fringe/margin.  With this variant, you may want to increase the
+value of `scroll-conservatively' to prevent the prompt from jumping.
+See also `erc-keep-place-indicator-divider'."
   :group 'erc
   :package-version '(ERC . "5.6")
   :type '(choice (const :tag "Use arrow" arrow)
                  (const :tag "Use face" face)
+                 (const :tag "Use separator on own line" divider)
                  (const :tag "Use both arrow and face" t)))
 
 (defcustom erc-keep-place-indicator-buffer-type t
@@ -337,9 +342,24 @@ erc-keep-place-indicator-arrow
   "Face for arrow value of option `erc-keep-place-indicator-style'."
   :group 'erc-faces)
 
+(defface erc-keep-place-indicator-divider
+  '((default :inherit (italic variable-pitch font-lock-constant-face)))
+  "Face for the variable `erc-keep-place-indicator-divider'."
+  :group 'erc-faces)
+
+(defvar erc-keep-place-indicator-divider
+  (propertize "\u2022 \u2022 \u2022" 'face 'erc-keep-place-indicator-divider)
+  "Label delineating the unread portion of the buffer.")
+
 (defvar-local erc--keep-place-indicator-overlay nil
   "Overlay for `erc-keep-place-indicator-mode'.")
 
+(defvar-local erc--keep-place-indicator-edge-triggered-p nil
+  "If non-nil, indicator moves to point when switching away from buffer.
+But only when the option `erc-keep-place-indicator-follow' is non-nil
+and point is greater than `window-start'.  The non-nil value should be a
+list of module symbols.")
+
 (defun erc--keep-place-indicator-on-window-buffer-change (_)
   "Maybe sync `erc--keep-place-indicator-overlay'.
 Do so only when switching to a new buffer in the same window if
@@ -358,7 +378,11 @@ erc--keep-place-indicator-on-window-buffer-change
               (prev (assq old-buffer (window-prev-buffers window)))
               (old-start (nth 1 prev))
               (old-inmkr (buffer-local-value 'erc-insert-marker old-buffer))
-              ((< (overlay-end ov) old-start old-inmkr)))
+              (old-point (nth 2 prev))
+              ((or (< (overlay-end ov) old-start old-inmkr)
+                   (and erc--keep-place-indicator-edge-triggered-p
+                        (< old-start old-point)
+                        (setq old-start old-point)))))
     (with-current-buffer old-buffer
       (erc-keep-place-move old-start))))
 
@@ -385,24 +409,10 @@ keep-place-indicator
                  #'erc--keep-place-indicator-adjust-on-clear '((depth . 40)))
    (if (pcase erc-keep-place-indicator-buffer-type
          ('target erc--target)
-         ('server (not erc--target))
+         ('server (null erc--target))
          ('t t))
-       (progn
-         (erc--restore-initialize-priors erc-keep-place-indicator-mode
-           erc--keep-place-indicator-overlay (make-overlay 0 0))
-         (when-let* (((memq erc-keep-place-indicator-style '(t arrow)))
-                     (ov-property (if (zerop (fringe-columns 'left))
-                                      'after-string
-                                    'before-string))
-                     (display (if (zerop (fringe-columns 'left))
-                                  `((margin left-margin) ,overlay-arrow-string)
-                                '(left-fringe right-triangle
-                                              erc-keep-place-indicator-arrow)))
-                     (bef (propertize " " 'display display)))
-           (overlay-put erc--keep-place-indicator-overlay ov-property bef))
-         (when (memq erc-keep-place-indicator-style '(t face))
-           (overlay-put erc--keep-place-indicator-overlay 'face
-                        'erc-keep-place-indicator-line)))
+       (erc--restore-initialize-priors erc-keep-place-indicator-mode
+         erc--keep-place-indicator-overlay (erc--keep-place-indicator-create))
      (erc-keep-place-indicator-mode -1)))
   ((when erc--keep-place-indicator-overlay
      (delete-overlay erc--keep-place-indicator-overlay))
@@ -424,6 +434,37 @@ keep-place-indicator
    (kill-local-variable 'erc--keep-place-indicator-overlay))
   localp)
 
+(defun erc--keep-place-indicator-create ()
+  "Regenerate `erc--keep-place-indicator-overlay'."
+  (let ((ov (make-overlay 0 0))
+        (marginp (zerop (fringe-columns 'left))))
+    (when-let*
+        (((memq erc-keep-place-indicator-style '(t arrow divider)))
+         (arrow (if marginp
+                    `((margin left-margin) ,overlay-arrow-string)
+                  '(left-fringe right-triangle
+                                erc-keep-place-indicator-arrow))))
+      (overlay-put ov (if marginp 'after-string 'before-string)
+                   (if-let*
+                       (((eq erc-keep-place-indicator-style 'divider))
+                        (w (if (and (display-graphic-p)
+                                    (fboundp 'string-pixel-width))
+                               (list (/ (string-pixel-width
+                                         erc-keep-place-indicator-divider)
+                                        2.0))
+                             (/ (length erc-keep-place-indicator-divider) 2)))
+                        (spec `(space :align-to
+                                      (+ (- center ,w) (0.5 . right-margin)))))
+                       (concat "\n" (and (display-graphic-p) "\n")
+                               (propertize " " 'display arrow)
+                               (propertize " " 'display spec)
+                               erc-keep-place-indicator-divider
+                               (and (not (display-graphic-p)) "\n"))
+                     (propertize " " 'display arrow))))
+    (when (memq erc-keep-place-indicator-style '(t face))
+      (overlay-put ov 'face 'erc-keep-place-indicator-line))
+    ov))
+
 (defun erc--keep-place-indicator-on-global-module ()
   "Ensure `keep-place-indicator' survives toggling `erc-keep-place-mode'.
 Do this by simulating `keep-place' in all buffers where
@@ -468,15 +509,17 @@ erc-keep-place-move
              (`(,_) (1- (min erc-insert-marker (window-end))))
              ('- (min (1- erc-insert-marker) (window-start)))))))
   (save-excursion
-    (let ((inhibit-field-text-motion t))
+    (let ((inhibit-field-text-motion t)
+          (dividerp (eq 'divider erc-keep-place-indicator-style))
+          beg end)
       (when pos
         (goto-char pos))
-      (when-let* ((pos (erc--get-inserted-msg-beg)))
-        (goto-char pos))
+      (let ((bounds (erc--get-inserted-msg-bounds)))
+        (goto-char (setq end (or (cdr bounds) (line-end-position))
+                         beg (or (car bounds) (line-beginning-position)))))
       (run-hooks 'erc--keep-place-move-hook)
       (move-overlay erc--keep-place-indicator-overlay
-                    (line-beginning-position)
-                    (line-end-position)))))
+                    (if dividerp end beg) (if dividerp (1+ end) end)))))
 
 (defun erc-keep-place-goto ()
   "Jump to keep-place indicator.
-- 
2.48.1

