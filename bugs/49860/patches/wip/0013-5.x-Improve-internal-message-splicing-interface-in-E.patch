From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Fri, 12 Apr 2024 23:51:36 -0700
Subject: [PATCH 13/37] [5.x] Improve internal message splicing interface in
 ERC

TODO: add tests.
TODO: add "healing/merging" message-deletion facility

* lisp/erc/erc-fill.el (erc-fill-wrap, erc-fill-wrap-enable,
erc-fill-wrap-disable): Add and remove
`erc-fill--wrap-mend-on-splice-insertion' from
`erc--insert-hook-splice-wrapper' locally.
(erc-fill--wrap-mend-sentinel): New variable.
(erc-fill--wrap-mend-on-splice-insertion): New function.
* lisp/erc/erc-stamp.el (erc-stamp--find-previous): New function.
(erc-stamp--delete-next): New function.
(erc-stamp--wrap-mend-on-splice-insertion): New function.
(erc-stamp--setup): Add and remove
`erc-stamp--wrap-mend-on-splice-insertion' from
`erc--insert-hook-splice-wrapper' locally.
* lisp/erc/erc.el (erc--insert-hook-splice-wrapper): New variable.
(erc--run-hook-member-and-ignore): New function.
(erc--with-spliced-insertion): New variable.
(erc--ignore-as-phony-hook-member): New function.
(erc--run-insert-hook): New macro.
(erc-insert-line): Use `erc--run-insert-hook'.
---
 lisp/erc/erc-fill.el  | 46 ++++++++++++++++++++++++++++++++++
 lisp/erc/erc-stamp.el | 58 ++++++++++++++++++++++++++++++++++++++++++-
 lisp/erc/erc.el       | 42 ++++++++++++++++++++++++-------
 3 files changed, 136 insertions(+), 10 deletions(-)

diff --git a/lisp/erc/erc-fill.el b/lisp/erc/erc-fill.el
index 9ccdae50f0d..1dac7d8a9ca 100644
--- a/lisp/erc/erc-fill.el
+++ b/lisp/erc/erc-fill.el
@@ -549,6 +549,8 @@ fill-wrap
    (add-function :after (local 'erc--clear-function)
                  #'erc-fill--wrap-massage-initial-message-post-clear
                  '((depth . 50)))
+   (add-function :around (local 'erc--insert-hook-splice-wrapper)
+                 #'erc-fill--wrap-mend-on-splice-insertion)
    (erc-stamp--display-margin-mode +1)
    (visual-line-mode +1))
   ((visual-line-mode -1)
@@ -561,6 +563,8 @@ fill-wrap
    (kill-local-variable 'erc-fill--wrap-merge-indicator-pre)
    (remove-function (local 'erc--clear-function)
                     #'erc-fill--wrap-massage-initial-message-post-clear)
+   (remove-function (local 'erc--insert-hook-splice-wrapper)
+                    #'erc-fill--wrap-mend-on-splice-insertion)
    (remove-hook 'erc--refresh-prompt-hook
                 #'erc-fill--wrap-indent-prompt t)
    (remove-hook 'erc-button--prev-next-predicate-functions
@@ -905,6 +909,48 @@ erc-fill-wrap-nudge
      1)
     (recenter line)))
 
+(defvar erc-fill--wrap-mend-sentinel
+  (list (make-symbol "erc--fill-wrap-mend")))
+
+(defun erc-fill--wrap-mend-on-splice-insertion
+    (orig hook-member hook-var &rest args)
+  "Call ORIG with ARGS when when HOOK-VAR is `erc-insert-modify-hook'.
+Adjust merged speakers on both sides of the splice divide."
+  (cl-assert erc--insert-marker)
+  ;; Intercept and deny date-stamp insertion on post-modify.
+  (cond ((eq hook-member #'erc-stamp--defer-date-insertion-on-post-insert)
+         nil)
+        ((and (eq hook-member #'erc-fill)
+              (null (erc--check-msg-prop 'erc--msg 'datestamp)))
+         (cl-assert (eq hook-var 'erc-insert-modify-hook))
+         (let ((erc-fill--wrap-last-msg
+                (save-restriction
+                  (widen)
+                  (and-let* ((found (previous-single-property-change
+                                     (1- erc--insert-marker) 'erc--spkr))
+                             (prev (erc--get-inserted-msg-beg found)))
+                    (copy-marker prev)))))
+           (push erc-fill--wrap-mend-sentinel erc--msg-prop-overrides)
+           (apply orig hook-member hook-var args)))
+        ((and (eq hook-var 'erc-insert-done-hook)
+              (eq #'erc--ignore-as-phony-hook-member hook-member)
+              (memq erc-fill--wrap-mend-sentinel erc--msg-prop-overrides))
+         (setq erc--msg-prop-overrides
+               (remq erc-fill--wrap-mend-sentinel erc--msg-prop-overrides))
+         (when-let*
+             ((pbounds (erc--get-inserted-msg-bounds (1- erc--insert-marker)))
+              ((get-text-property (car pbounds) 'erc--spkr))
+              ((get-text-property erc--insert-marker 'erc--spkr))
+              (nbounds (erc--get-inserted-msg-bounds erc--insert-marker))
+              (erc-fill--wrap-rejigger-last-message
+               (copy-marker (car pbounds))))
+           (save-excursion
+             (with-silent-modifications
+               (erc-fill--wrap-rejigger-region (car nbounds) (1+ (cdr nbounds))
+                                               nil 'repairp))))
+         (apply orig hook-member hook-var args))
+        (t (apply orig hook-member hook-var args))))
+
 (defun erc-fill-regarding-timestamp ()
   "Fills a text such that messages start at column `erc-fill-static-center'."
   (fill-region (point-min) (point-max) t t)
diff --git a/lisp/erc/erc-stamp.el b/lisp/erc/erc-stamp.el
index 79fd4ecaacd..5578bac5810 100644
--- a/lisp/erc/erc-stamp.el
+++ b/lisp/erc/erc-stamp.el
@@ -762,6 +762,58 @@ erc-stamp--defer-date-insertion-on-post-modify
                 (setq erc-timestamp-last-inserted-left rendered)))))
     (add-hook hook-var symbol -90)))
 
+(defun erc-stamp--find-previous (pos)
+  "Find previous timestamp before POS that's not a datestamp."
+  (let ((p pos)
+        beg)
+    (while (and p (setq p (previous-single-property-change p 'field))
+                (not (and-let* (((eq 'erc-timestamp
+                                     (or (field-at-pos p)
+                                         (field-at-pos (1- p)))))
+                                (b (erc--get-inserted-msg-beg p))
+                                ((not (and (eq (get-text-property b 'erc--msg)
+                                               'datestamp)
+                                           (always (setq p nil)))))
+                                ((setq beg b)))))))
+    (when beg
+      (get-text-property beg 'erc--ts))))
+
+(defun erc-stamp--delete-next (pos reference)
+  (when-let* ((next (text-property-any (1- pos) erc-insert-marker 'field
+                                       'erc-timestamp))
+              ((eq 'erc-timestamp (field-at-pos (1+ next))))
+              (bounds (erc--get-inserted-msg-bounds next))
+              ((not (eq (get-text-property (car bounds) 'erc--msg) 'datestamp)))
+              ((save-excursion
+                 (goto-char (car bounds))
+                 (search-forward reference (cdr bounds) t))))
+    (with-silent-modifications
+      (delete-region (field-beginning (1+ next)) (field-end (1+ next))))))
+
+(defun erc-stamp--wrap-mend-on-splice-insertion (orig member hook &rest args)
+  "Run HOOK MEMBER via ORIG with ARGS, shadowing \"last-inserted\" vars."
+  (if (and (eq member #'erc-add-timestamp)
+           erc-timestamp-only-if-changed-flag erc-timestamp-format)
+      (let* ((ts (save-excursion
+                   (save-restriction
+                     (widen)
+                     (erc-stamp--find-previous erc--insert-marker))))
+             str
+             (erc-timestamp-last-inserted
+              (and ts erc-timestamp-last-inserted
+                   (setq str (erc-format-timestamp ts erc-timestamp-format))))
+             (erc-timestamp-last-inserted-right
+              (and ts erc-timestamp-last-inserted-right
+                   (or str (erc-format-timestamp ts erc-timestamp-format)))))
+        (prog1 (apply orig member hook args)
+          (when-let* ((existing (or erc-timestamp-last-inserted
+                                    erc-timestamp-last-inserted-right)))
+            (save-excursion
+              (save-restriction
+                (widen)
+                (erc-stamp--delete-next erc--insert-marker existing))))))
+    (apply orig member hook args)))
+
 (defun erc-stamp--defer-date-insertion-on-post-insert ()
   (erc-stamp--defer-date-insertion-on-post-modify 'erc-timer-hook))
 
@@ -980,13 +1032,17 @@ erc-stamp--setup
       (progn
         (erc-stamp--manage-local-options-state)
         (add-function :around (local 'erc--clear-function)
-                      #'erc-stamp--reset-on-clear '((depth . 40))))
+                      #'erc-stamp--reset-on-clear '((depth . 40)))
+        (add-function :around (local 'erc--insert-hook-splice-wrapper)
+                      #'erc-stamp--wrap-mend-on-splice-insertion))
     (let (erc-echo-timestamps erc-hide-timestamps erc-timestamp-intangible)
       (erc-stamp--manage-local-options-state))
     ;; Undo local mods from `erc-insert-timestamp-left-and-right'.
     (erc-stamp--date-mode -1) ; kills `erc-timestamp-last-inserted-left'
     (remove-function (local 'erc--clear-function)
                      #'erc-stamp--reset-on-clear)
+    (remove-function (local 'erc--insert-hook-splice-wrapper)
+                     #'erc-stamp--wrap-mend-on-splice-insertion)
     (kill-local-variable 'erc-stamp--last-stamp)
     (kill-local-variable 'erc-timestamp-last-inserted)
     (kill-local-variable 'erc-timestamp-last-inserted-right)
diff --git a/lisp/erc/erc.el b/lisp/erc/erc.el
index cd963e6c8c3..d9a35d4990b 100644
--- a/lisp/erc/erc.el
+++ b/lisp/erc/erc.el
@@ -3425,11 +3425,14 @@ erc--get-inserted-msg-prop
   (and-let* ((stack-pos (erc--get-inserted-msg-beg (or point (point)))))
     (get-text-property stack-pos prop)))
 
-;; FIXME improve this nascent "message splicing" facility to include a
-;; means for modules to adjust inserted messages on either side of the
-;; splice position as well as to modify the spliced-in message itself
-;; before and after each insertion-related hook runs.  Also add a
-;; counterpart to `erc--with-spliced-insertion' for deletions.
+(defvar erc--insert-hook-splice-wrapper
+  #'erc--run-hook-member-and-ignore
+  "Function to update the appearance of messages when splicing.")
+
+(defun erc--run-hook-member-and-ignore (function _ &rest args)
+  (apply function args)
+  nil)
+
 (defvar erc--insert-line-splice-function
   #'erc--insert-before-markers-transplanting-hidden
   "Function to handle in-place insertions away from prompt.
@@ -3453,6 +3456,7 @@ erc--with-spliced-insertion
        (let* ((,marker (and (not (markerp ,marker-or-pos))
                             (copy-marker ,marker-or-pos)))
               (erc--insert-marker (or ,marker ,marker-or-pos))
+              (erc--insert-hook-wrapper erc--insert-hook-splice-wrapper)
               (erc--insert-line-function erc--insert-line-splice-function))
          (prog1 (progn ,@body)
            (when ,marker (set-marker ,marker nil)))))))
@@ -3501,6 +3505,11 @@ erc--insert-line-function
 inserted at that position.  This can be addressed by binding this
 variable to `insert-before-markers' around calls to `display-message'.")
 
+(defvar erc--insert-hook-wrapper nil
+  "When non-nil, wrap hooks called by `erc-insert-line' with value.
+The wrapper must accept an additional argument between FUN and ARGS: the
+symbol of the hook being run.")
+
 (defvar erc--insert-marker nil
   "Internal override for `erc-insert-marker'.")
 
@@ -3518,6 +3527,21 @@ erc--with-offset-marker
      (when erc--offset-marker
        (set-marker erc--offset-marker nil))))
 
+(defalias 'erc--ignore-as-phony-hook-member #'ignore)
+
+(defmacro erc--run-insert-hook (hook &rest args)
+  "Run HOOK, a quoted symbol, with ARGS.
+When `erc--insert-hook-wrapper' is non-nil, run HOOK's members via its
+value, a `run-hook-wrapped'-compatible function, which always runs at
+least once, courtesy of `erc--ignore-as-phony-hook-member', even when
+HOOK's value is empty."
+  (cl-assert (eq (car-safe hook) 'quote))
+  `(if erc--insert-hook-wrapper
+       (progn (run-hook-wrapped ,hook erc--insert-hook-wrapper ,hook ,@args)
+              (funcall erc--insert-hook-wrapper
+                       #'erc--ignore-as-phony-hook-member ,hook ,@args))
+     (,(if args 'run-hook-with-args 'run-hooks) ,hook ,@args)))
+
 (define-obsolete-function-alias 'erc-display-line-1 'erc-insert-line "30.1")
 (defun erc-insert-line (string buffer)
   "Insert STRING in an `erc-mode' BUFFER.
@@ -3555,7 +3579,7 @@ erc-insert-line
                            (format "(%S)" string) " in buffer "
                            (format "%s" buffer)))
           (setq erc-insert-this t)
-          (run-hook-with-args 'erc-insert-pre-hook string)
+          (erc--run-insert-hook 'erc-insert-pre-hook string)
           (setq insert-position (marker-position (or erc--insert-marker
                                                      erc-insert-marker)))
           (if (null erc-insert-this)
@@ -3576,8 +3600,8 @@ erc-insert-line
                 (save-restriction
                   (narrow-to-region insert-position (point))
                   (erc--with-offset-marker
-                   (run-hooks 'erc-insert-modify-hook)
-                   (run-hooks 'erc-insert-post-hook))
+                   (erc--run-insert-hook 'erc-insert-modify-hook)
+                   (erc--run-insert-hook 'erc-insert-post-hook))
                   (when erc-remove-parsed-property
                     (remove-text-properties (point-min) (point-max)
                                             '(erc-parsed nil tags nil)))
@@ -3588,7 +3612,7 @@ erc-insert-line
                                  '(erc--msg unknown))))
                     (add-text-properties (point-min) (1+ (point-min)) props)))
                 (erc--refresh-prompt)))))
-        (run-hooks 'erc-insert-done-hook)
+        (erc--run-insert-hook 'erc-insert-done-hook)
         (erc-update-undo-list (- (or erc--insert-marker erc-insert-marker
                                      (point-max))
                                  insert-position))))))
-- 
2.48.1

