From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Sun, 24 Nov 2024 22:22:58 -0800
Subject: [PATCH 02/37] [5.7] Make ERC a special mode

* lisp/erc/erc.el: Set the `mode-class' property of `erc-mode' to
`special' to allow users to associate ERC buffers with a file.  Prior to
this, an attempt to `save-buffer' or `write-file' would corrupt the
application state by leaving references to non-ERC buffers in
`erc-server-users'.
(erc--restore-unvisited-name): New function to prevent ERC from renaming
the buffer to the visited file's name.  The original name is managed by
erc-networks and necessary for disambiguation.
(erc-mode): Add `erc--restore-unvisited-name' to
`after-set-visited-file-name-hook' locally.
---
 lisp/erc/erc.el | 22 ++++++++++++++++++++++
 1 file changed, 22 insertions(+)

diff --git a/lisp/erc/erc.el b/lisp/erc/erc.el
index 338e3b18b1f..b8ce2fc87ed 100644
--- a/lisp/erc/erc.el
+++ b/lisp/erc/erc.el
@@ -1779,6 +1779,24 @@ erc-set-active-buffer
            (setq erc-active-buffer buffer)))
         (t (setq erc-active-buffer buffer))))
 
+(defun erc--restore-unvisited-name ()
+  "Restore buffer's preferred name if currently that of its visited file."
+  (when-let*
+      (((derived-mode-p 'erc-mode))
+       (buffer-file-name)
+       (last-name (buffer-last-name))
+       ((not (equal (buffer-name) last-name)))
+       ((equal (buffer-name) (file-name-nondirectory buffer-file-name))))
+    ;; FIXME erase after verifying it still holds in query buffers where
+    ;; the target has renicked.
+    (cl-assert (and erc-networks--id
+                    (equal last-name
+                           (if erc--target
+                               (erc-networks--reconcile-buffer-names
+                                erc--target erc-networks--id)
+                             (erc-networks--id-string erc-networks--id)))))
+    (rename-buffer last-name)))
+
 ;; Mode activation routines
 
 (define-derived-mode erc-mode fundamental-mode "ERC"
@@ -1795,9 +1813,13 @@ erc-mode
   (add-hook 'post-command-hook #'erc-check-text-conversion nil t)
   (add-hook 'kill-buffer-hook #'erc-kill-buffer-function nil t)
   (add-hook 'completion-at-point-functions #'erc-complete-word-at-point nil t)
+  (add-hook 'after-set-visited-file-name-hook
+            #'erc--restore-unvisited-name nil t)
   (add-function :before (local 'erc--clear-function)
                 #'erc--skip-past-headroom-on-clear '((depth . 30))))
 
+(put 'erc-mode 'mode-class 'special)
+
 ;; activation
 
 (defconst erc-default-server "irc.libera.chat"
-- 
2.48.1

