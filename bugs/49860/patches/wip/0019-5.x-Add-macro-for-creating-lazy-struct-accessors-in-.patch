From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Mon, 4 Mar 2024 06:15:31 -0800
Subject: [PATCH 19/37] [5.x] Add macro for creating lazy struct accessors in
 ERC

When initializing a `cl-struct' instance, it's often handy to use an
alternate constructor that encapsulates common slot-setup code.  This
change provides an extended `cl-struct' type that initializes slots on
first use.  One major downside is that definitions for new response
types create more than twice the amount of byte code.

* lisp/erc/erc-common.el (erc--define-lazy-accessors): New macro.
(erc--define-lazy-struct): New macro.
* test/lisp/erc/erc-tests.el (erc-tests--my-struct): New struct type for
testing purposes.
(erc--define-lazy-struct-accessors): New test.
---
 lisp/erc/erc-common.el     | 140 +++++++++++++++++++++++++++++++++++++
 test/lisp/erc/erc-tests.el |  46 ++++++++++++
 2 files changed, 186 insertions(+)

diff --git a/lisp/erc/erc-common.el b/lisp/erc/erc-common.el
index 26c656d8c50..f5d1a34b42b 100644
--- a/lisp/erc/erc-common.el
+++ b/lisp/erc/erc-common.el
@@ -707,6 +707,146 @@ erc--doarray
          (cl-incf ,i))
        ,(nth 2 spec))))
 
+;; Despite `require'ing `cl-lib' at the top of this file we still incur
+;; warnings that these may not be defined at runtime.
+(declare-function cl-struct-slot-info "cl-macs" (struct-type))
+(declare-function cl-struct-slot-offset "cl-macs" (struct-type slot-name))
+
+(defmacro erc--define-lazy-accessors (name parent-name sentinel &rest parts)
+  ;; The struct NAME will already be defined so long as this macro call
+  ;; appears after NAME's definition.
+  (declare (indent 3))
+  (let ((val-var (make-symbol "val"))
+        (place-var (make-symbol "place"))
+        (defuns ())
+        (supers ()))
+    ;; Add an alias for slots of parent not in PARTS.
+    (when parent-name
+      (dolist (slot (cdr (cl-struct-slot-info name)))
+        (when-let*
+            ((slot-name (car slot))
+             ((null (assq slot-name parts)))
+             (acsr (intern (format "%s-%s" name slot-name))))
+          (let ((acsr-par (intern-soft (format "%s-%s" parent-name slot-name)))
+                (acsr-priv (intern (format "%s--%s" name slot-name))))
+            (push (list slot-name acsr (if (and acsr-par (fboundp acsr-par))
+                                           acsr-par
+                                         acsr-priv))
+                  supers))))
+      (setq parts (append parts supers)))
+    (pcase-dolist (`(,slot ,acsr ,acsr-priv ,init-form, inst-var) parts)
+      ;; For readability and slightly smaller byte code, forgo type
+      ;; checking and just call `aset' and `aref' directly.  In
+      ;; anecdotal trials, it doesn't significantly improve performance.
+      ;; Also ensure that wherever `init-form' appears, `inst-var' is
+      ;; bound to the instance object.
+      (push
+       (if-let* ((inst-var)
+                 (offset (cl-struct-slot-offset name slot))
+                 (esc-var (list backquote-unquote-symbol inst-var)))
+           ;; Define an explicit expander because `gv-get' doesn't seem
+           ;; to like an `aset' for the last form of the body.
+           `(define-inline ,acsr (,inst-var)
+              ,(concat
+                (format "Access slot \"%s\" of `%s' INSTANCE." slot name)
+                "\nAs a side effect, also initialize the slot if unset.")
+              (declare
+               (gv-expander
+                (lambda (do)
+                  (funcall
+                   do (,backquote-backquote-symbol
+                       (let* ((,place-var ,esc-var)
+                              (,val-var (aref ,place-var ,offset)))
+                         (if (eq ,val-var ,sentinel)
+                             (aset ,place-var ,offset
+                                   (let ((,inst-var ,place-var)) ,init-form))
+                           ,val-var)))
+                   (lambda (val)
+                     (,backquote-backquote-symbol
+                      (aset ,esc-var ,offset
+                            (,backquote-unquote-symbol val))))))))
+              (inline-letevals (,inst-var)
+                (inline-quote
+                 (let ((,val-var (aref ,esc-var ,offset)))
+                   (if (eq ,val-var ,sentinel)
+                       (aset ,esc-var ,offset (let ((,inst-var ,esc-var))
+                                                ,init-form))
+                     ,val-var)))))
+         `(defalias ',acsr #',acsr-priv))
+       defuns))
+    ;; Macros referencing these functions need this evaluated.
+    `(eval-and-compile ,@defuns)))
+
+(defmacro erc--define-lazy-struct (name-and-options &rest slots)
+  "Define a cl-struct capable of lazily initializing its slots.
+Expect NAME-AND-OPTIONS to be either a symbol with which to NAME the
+struct or a list headed by NAME and followed by `cl-defstruct' OPTIONS.
+Expect SLOTS to be a list of the struct's slot definitions, with those
+intended for lazy initialization marked by a nonstandard `:lazy' keyword
+\(more on that below).  For each lazy SLOT in SLOTS, create an
+\"internal\" (double-hyphenated) accessor function named NAME--SLOT as
+well as a \"public\" accessor function, NAME-SLOT, that defers
+initialization until first use.  If OPTIONS contains an :include
+directive, alias public accessors for slots not appearing in SLOTS to
+the parent's corresponding public accessors.
+
+Example usage:
+  (erc--define-lazy-struct my-struct
+    (foo (+ 1 2 3) :type integer)
+    (bar (+ 4 5 6) :type integer :lazy t)
+    (baz (my-struct-p inst) :type boolean :lazy inst))
+
+When code calls (my-struct-bar INSTANCE) for the first time, ERC
+initializes INSTANCE's `bar' slot with the result of its initialization
+form, (+ 4 5 6).  On subsequent calls, ERC returns the stashed value.
+The same goes for `baz', except its init form references the current
+instance.  ERC treats slot `foo' as a normal, non-lazy slot."
+  (declare (indent defun))
+  (cl-assert cl--struct-inline)
+  (let* ((name (car (ensure-list name-and-options)))
+         (sentinel (intern (format "%s-empty" name)))
+         (options (and (listp name-and-options) (cdr name-and-options)))
+         (parent-name (cadr (assq :include options)))
+         ;;
+         def-parts adjusted-slots)
+    (cl-assert (null (assq :conc-name options)))
+    (push `(:conc-name ,(intern (concat (symbol-name name) "--"))) options)
+    ;; Process slots, massaging those marked as `:lazy' for deferred
+    ;; initialization.
+    (dolist (slot slots)
+      (if-let* (((consp slot))
+                (found (memq :lazy slot)))
+          (pcase-let ((`(,slot-name ,init-form . ,rest) slot))
+            (setq rest (append (butlast rest (length found)) (cddr found)))
+            (let ((inst-var (cadr found))
+                  (acsr (intern (format "%s-%s" name slot-name)))
+                  (acsr-priv (intern (format "%s--%s" name slot-name))))
+              (when (or (memq inst-var '(nil t)) (keywordp inst-var)
+                        (not (symbolp inst-var)))
+                (setq inst-var (make-symbol "inst-var")))
+              ;; Ensure we have the right kind of struct.
+              (push `(,slot-name ,acsr ,acsr-priv ,init-form, inst-var)
+                    def-parts)
+              (setq slot `(,slot-name ,sentinel ,@rest))))
+        ;; Arrange for aliasing accessors for normal slots.
+        (let* ((slot-name (car (ensure-list slot)))
+               (acsr (intern (format "%s-%s" name slot-name)))
+               (acsr-priv (intern (format "%s--%s" name slot-name))))
+          (push (list slot-name acsr acsr-priv) def-parts)))
+      (push slot adjusted-slots))
+    ;; Recursively inspect super classes and create a defalias for all
+    ;; slots to the public name, sans double hyphen.
+    `(progn
+       (defvar ,sentinel (make-symbol "--uninitialized--"))
+       (cl-defstruct ,(cons name options)
+         "A struct supporting lazy slot initialization."
+         ,@(nreverse adjusted-slots))
+       ;; Couch definitions that depend on record offsets from the above
+       ;; `cl-defstruct' in a separate macro.
+       (erc--define-lazy-accessors ,name ,parent-name ,sentinel
+         ,@(nreverse def-parts))
+       ',name)))
+
 (provide 'erc-common)
 
 ;;; erc-common.el ends here
diff --git a/test/lisp/erc/erc-tests.el b/test/lisp/erc/erc-tests.el
index 818bd9d5a83..dec88f70b9f 100644
--- a/test/lisp/erc/erc-tests.el
+++ b/test/lisp/erc/erc-tests.el
@@ -4042,6 +4042,52 @@ define-erc-module--local
                       (put 'erc-mname-enable 'definition-name 'mname)
                       (put 'erc-mname-disable 'definition-name 'mname))))))
 
+(erc--define-lazy-struct erc-tests--my-struct
+  (foo (list 1 2 3) :type (list-of integer))
+  (bar (erc-tests--my-struct-p o) :lazy o))
+
+(ert-deftest erc--define-lazy-accessors ()
+  :tags '(:unstable)
+
+  ;; Ensure asserted input matches generated form.
+  (let ((def (car (last (macroexpand
+                         '(erc--define-lazy-struct erc-tests--my-struct
+                            (foo (list 1 2 3) :type (list-of integer))
+                            (bar (erc-tests--my-struct-p o) :lazy o)))
+                        2))))
+    (should (equal def
+                   '(erc--define-lazy-accessors erc-tests--my-struct nil
+                                                erc-tests--my-struct-empty
+                      (foo erc-tests--my-struct-foo erc-tests--my-struct--foo)
+                      (bar erc-tests--my-struct-bar erc-tests--my-struct--bar
+                           (erc-tests--my-struct-p o) o))))
+
+    (should
+     (equal
+      (read (prin1-to-string (macroexpand-1 def)))
+      '(eval-and-compile
+         (define-inline erc-tests--my-struct-bar (o)
+           "Access slot \"bar\" of `erc-tests--my-struct' INSTANCE.
+As a side effect, also initialize the slot if unset."
+           (declare (gv-expander
+                     (lambda (do)
+                       (funcall do
+                                `(let* ((place ,o)
+                                        (val (aref place 2)))
+                                   (if (eq val erc-tests--my-struct-empty)
+                                       (aset place 2
+                                             (let ((o place))
+                                               (erc-tests--my-struct-p o)))
+                                     val))
+                                (lambda (val) `(aset ,o 2 ,val))))))
+           (inline-letevals (o)
+             (inline-quote
+              (let ((val (aref ,o 2)))
+                (if (eq val erc-tests--my-struct-empty)
+                    (aset ,o 2 (let ((o ,o)) (erc-tests--my-struct-p o)))
+                  val)))))
+         (defalias 'erc-tests--my-struct-foo #'erc-tests--my-struct--foo))))))
+
 (ert-deftest erc-tests-common-string-to-propertized-parts ()
   :tags '(:unstable) ; only run this locally
   (unless (>= emacs-major-version 28) (ert-skip "Missing `object-intervals'"))
-- 
2.48.1

