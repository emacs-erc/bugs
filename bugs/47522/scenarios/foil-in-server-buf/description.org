
:PROPERTIES:
:Source: debbugs
:Originator: Muto <shack@muto.ca>
:Number: 47522
:Message-Id: <87im57a06m.fsf@muto.ca>
:Emacs-version: 27.2
:Subject: Channel Buffers Should Be Unique
:Date: Apr 06, 2021
:Proxy: false
:END:

* Description
See the scenario "ambiguous-join" for a simulation of the /behavior/
described in the bug report (likely not a bug). This follows the
/instructions/ described in that report, but the result does not match
what's reported. Steps follow.

Connect to a network and join some channel =#chan=. Connect to another
network. Attempt to join /its/ =#chan= from the server buffer created
for this /second/ network. Unlike what's stated in the bug report, the
connection happens and another channel buffer, something like a
=#chan/<ip address>:<port>= is created and displayed.
