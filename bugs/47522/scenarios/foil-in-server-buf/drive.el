
(require 'repro)

(ert-deftest repro-test ()
  (repro-with-inferior

   (repro-term-speak 3 "Going in 3 ...")

   (repro-execute-bug-recipe)

   (ert-info ("Connect to first server")
     (repro-term-await-buffer "foonet" 10)
     (repro-term-speak 6 "This should be foonet with %S" 'erc-server-process))

   (ert-info ("Join #chan@foonet")
     (repro-term-await-buffer "#chan@foonet" 6)
     (repro-term-await-content (rx (or "alice" "bob")) 4)
     (should (string= "#<buffer #chan>" (repro-term-eval '(current-buffer))))
     (repro-term-speak 6 "This should be #chan@foonet %S" '(current-buffer)))

   (ert-info ("Connect to second server")
     (repro-term-await-buffer "barnet" 10)
     (repro-term-speak 5 4 "This should be barnet with %S"
                       'erc-server-process))

   (ert-info ("Join #chan@barnet")
     (repro-term-await-buffer "#chan@barnet" 6)
     (should (string= "#<buffer #chan/127.0.0.1<2>>"
                      (repro-term-eval '(current-buffer))))
     (repro-term-await-content (rx (or "joe" "mike")) 4)
     (repro-term-speak 6 "This should be #chan@barnet %S" '(current-buffer)))

   (ert-info ("First #chan buffer has been renamed")
     (should (string= "nil" (repro-term-eval '(get-buffer "#chan"))))
     (repro-term-speak 6 "Buffer #chan has been renamed to %s"
                       '(get-buffer "#chan/127.0.0.1")))

   (sleep-for 5)
   (repro-term-eval '(ibuffer))
   (sleep-for 1)
   (term-send-raw-string "g")
   (repro-term-speak 6 "There should be two servers and two #chan variants")

   (sleep-for 2)))
