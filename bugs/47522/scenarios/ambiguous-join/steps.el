(require 'erc)

(cl-macrolet ((at (d &rest b) `(run-at-time ,d nil (lambda () (progn ,@b)))))

  (pop-to-buffer-same-window (erc :server "127.0.0.1"
                                  :port 6667
                                  :nick "tester"
                                  :password "changeme"
                                  :full-name "tester"))

  (at 5 (execute-kbd-macro "/join #chan"))
  (at 6 (execute-kbd-macro "\r"))

  (at 10 (pop-to-buffer-same-window (erc :server "127.0.0.1"
                                         :port 6668
                                         :nick "tester"
                                         :password "changeme"
                                         :full-name "tester")))

  (at 15 (pop-to-buffer-same-window "127.0.0.1:6667"))

  (at 18 (execute-kbd-macro "\e>/join #chan"))
  (at 19 (execute-kbd-macro "\r"))

  (at 23 (execute-kbd-macro "\e>/join #chan"))
  (at 24 (execute-kbd-macro "\r")))
