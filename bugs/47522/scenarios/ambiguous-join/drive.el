
(require 'repro)

(ert-deftest repro-test ()
  (repro-with-inferior

   (repro-term-speak 3 "Going in 3 ...")

   (repro-execute-bug-recipe)

   (ert-info ("Connect to first server")
     (repro-term-await-buffer "foonet" 10)
     (repro-term-speak 6 "This should be foonet with %S" 'erc-server-process))

   (ert-info ("Join #chan@foonet")
     (repro-term-await-buffer "#chan@foonet" 6)
     (repro-term-await-content (rx (or "alice" "bob")) 4)
     (should (string= "#<buffer #chan>" (repro-term-eval '(current-buffer))))
     (repro-term-speak 5 "This should be #chan@foonet %S" '(current-buffer)))

   (ert-info ("Connect to second server")
     (repro-term-await-buffer "barnet" 10)
     (repro-term-speak 4 4 "This should be barnet with %S"
                       'erc-server-process))

   (ert-info ("Back to first server")
     (repro-term-speak 2 4 "Back to foonet"))

   (ert-info ("Join #chan (again)")
     (repro-term-await-buffer "#chan@foonet" 6)
     (should (string= "#<buffer #chan>" (repro-term-eval '(current-buffer))))
     (repro-term-await-content-invert (rx (or "joe" "mike")) 1)
     (repro-term-speak 4 5 "This should (again) be #chan@foonet %S"
                       '(current-buffer))
     (repro-term-speak 3 5 "Joining from here (also) does nothing"))

   (sleep-for 5)
   (repro-term-eval '(ibuffer))
   (sleep-for 1)
   (term-send-raw-string "g")
   (repro-term-speak 6 "There should be two servers and a one #chan")

   (sleep-for 2)))
