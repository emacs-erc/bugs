From 51425461c8fe0e49ceb86890e16aa02a40407b3a Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Sun, 6 Oct 2024 23:17:40 -0700
Subject: [PATCH 1/3] [5.7] Use speaker-end marker in ERC insertion hooks

* lisp/erc/erc-fill.el (erc-fill-wrap): Use `erc--offset-marker' instead
of heuristics for finding the beginning of the message proper.
* lisp/erc/erc.el (erc--send-action-display): Use
`erc--ensure-offset-prop'.
(erc--ensure-offset-prop): New function.  Only works for
`erc--message-speaker-catalog' entries, which all (currently) end in
"%m".  If any were to gain a "footer" component after their "%m", this
would need to be modified, possibly to require an extra `catalog-key'
parameter that could then be queried at runtime for a symbol property
specifying the footer length as a negative offset.
(erc--add-msg-prop): New function.
(erc--offset-marker): New variable.
(erc--with-offset-marker): New macro.
(erc-insert-line): Run insertion hooks in `erc--with-offset-marker'.
(erc--determine-speaker-message-format-args)
(erc--format-speaker-input-message)
(erc-ctcp-query-ACTION): Use `erc--ensure-offset-prop'.  In the latter,
don't set statusmsg "%s" to the target name.
(erc-make-notice): Set `erc--offset' msg prop to the length of the
`erc--notice-prefix', which includes a trailing space.  Don't do the
same for the fallback case of `erc-display-message-highlight' because
some format specs contain leading characters that are basically analogs
of `erc-notice-prefix'.  Examining each prematurely to formulate a guess
that may never be used is wasteful, and just going with 0 would
sometimes be wrong or destructive, such as on subsequent passes for
"compound" `erc-display-message' type parameters specified by
`erc-display-error-notice', etc.
(erc-display-msg): Run send hooks in `erc--with-offset-marker'.
(Bug#73798)
---
 lisp/erc/erc-fill.el | 20 ++++++++++--------
 lisp/erc/erc.el      | 48 +++++++++++++++++++++++++++++++++++---------
 2 files changed, 51 insertions(+), 17 deletions(-)

diff --git a/lisp/erc/erc-fill.el b/lisp/erc/erc-fill.el
index 145a4c174a8..9ccdae50f0d 100644
--- a/lisp/erc/erc-fill.el
+++ b/lisp/erc/erc-fill.el
@@ -707,14 +707,18 @@ erc-fill-wrap
                         (funcall erc-fill--wrap-length-function))
                    (and-let* ((msg-prop (erc--check-msg-prop 'erc--msg))
                               ((not (eq msg-prop 'unknown))))
-                     (when-let* ((e (erc--get-speaker-bounds))
-                                 (b (pop e))
-                                 ((or erc-fill--wrap-action-dedent-p
-                                      (not (erc--check-msg-prop 'erc--ctcp
-                                                                'ACTION)))))
-                       (goto-char e))
-                     (skip-syntax-forward "^-")
-                     (forward-char)
+                     (let ((dedentp (or erc-fill--wrap-action-dedent-p
+                                        (not (erc--check-msg-prop 'erc--ctcp
+                                                                  'ACTION)))))
+                       (if (and dedentp erc--offset-marker)
+                           (goto-char erc--offset-marker)
+                         ;; No marker means `datestamp' or refilling via
+                         ;; `erc-fill--wrap-unmerge-on-date-stamp', etc.
+                         (when-let* ((dedentp)
+                                     (bounds (erc--get-speaker-bounds)))
+                           (goto-char (cdr bounds)))
+                         (skip-syntax-forward "^-")
+                         (forward-char)))
                      (cond ((eq msg-prop 'datestamp)
                             (when erc-fill--wrap-rejigger-last-message
                               (set-marker erc-fill--wrap-last-msg (point-min)))
diff --git a/lisp/erc/erc.el b/lisp/erc/erc.el
index d377839733b..cca9a395ed4 100644
--- a/lisp/erc/erc.el
+++ b/lisp/erc/erc.el
@@ -3190,7 +3190,8 @@ erc--send-action-display
       (let ((erc-current-message-catalog erc--message-speaker-catalog))
         (erc-display-message nil nil (current-buffer) 'ctcp-action-input
                              ?p (erc-get-channel-membership-prefix nick)
-                             ?n (erc--speakerize-nick nick) ?m string)))))
+                             ?n (erc--speakerize-nick nick)
+                             ?m (erc--ensure-offset-prop string))))))
 
 (defun erc--send-action (target string force)
   "Display STRING, then send to TARGET as a \"CTCP ACTION\" message."
@@ -3214,6 +3215,11 @@ erc--ensure-spkr-prop
                `((erc--spkr . ,nick) ,@overrides ,@erc--msg-prop-overrides))))
   nick)
 
+(defun erc--ensure-offset-prop (message)
+  "Add `erc--offset' msg prop for string MESSAGE."
+  (erc--add-msg-prop 'erc--offset (- (length message)))
+  message)
+
 (defun erc-string-invisible-p (string)
   "Check whether STRING is invisible or not.
 I.e. any char in it has the `invisible' property set."
@@ -3328,6 +3334,13 @@ erc--memq-msg-prop
              ((consp haystack)))
     (memq needle haystack)))
 
+(defun erc--add-msg-prop (prop val)
+  "Add PROP and VAL to `erc--msg-props' or `erc--msg-prop-overrides'."
+  (cond (erc--msg-props
+         (puthash prop val erc--msg-props))
+        (erc--msg-prop-overrides
+         (setf (alist-get prop erc--msg-prop-overrides) val))))
+
 (defmacro erc--get-inserted-msg-beg-at (point at-start-p)
   (macroexp-let2* nil ((point point)
                        (at-start-p at-start-p))
@@ -3452,6 +3465,20 @@ erc--insert-line-function
 (defvar erc--insert-marker nil
   "Internal override for `erc-insert-marker'.")
 
+(defvar erc--offset-marker nil
+  "Demarcates the header/body partition in a message.")
+
+(defmacro erc--with-offset-marker (&rest body)
+  "Run BODY in insertion-narrowed buffer with `erc--offset-marker' present."
+  `(let ((erc--offset-marker
+          (and-let* ((offset (erc--check-msg-prop 'erc--offset))
+                     (side (if (natnump offset) (point-min) (1- (point-max)))))
+            (remhash 'erc--offset erc--msg-props)
+            (copy-marker (+ side offset)))))
+     ,@body
+     (when erc--offset-marker
+       (set-marker erc--offset-marker nil))))
+
 (define-obsolete-function-alias 'erc-display-line-1 'erc-insert-line "30.1")
 (defun erc-insert-line (string buffer)
   "Insert STRING in an `erc-mode' BUFFER.
@@ -3509,8 +3536,9 @@ erc-insert-line
                 ;; run insertion hook, with point at restored location
                 (save-restriction
                   (narrow-to-region insert-position (point))
-                  (run-hooks 'erc-insert-modify-hook)
-                  (run-hooks 'erc-insert-post-hook)
+                  (erc--with-offset-marker
+                   (run-hooks 'erc-insert-modify-hook)
+                   (run-hooks 'erc-insert-post-hook))
                   (when erc-remove-parsed-property
                     (remove-text-properties (point-min) (point-max)
                                             '(erc-parsed nil tags nil)))
@@ -6437,7 +6465,7 @@ erc--determine-speaker-message-format-args
                      (if inputp 'input-query-notice 'query-notice)
                    (if inputp 'input-chan-notice 'chan-notice))))
         ?p (or prefix "") ?n (erc--speakerize-nick nick disp-nick)
-        ?s (or statusmsg "") ?m message))
+        ?s (or statusmsg "") ?m (erc--ensure-offset-prop message)))
 
 (defcustom erc-show-speaker-membership-status nil
   "Whether to prefix speakers with their channel status.
@@ -6571,7 +6599,7 @@ erc--format-speaker-input-message
               (erc--msg-prop-overrides (push (cons 'erc--msg key)
                                              erc--msg-prop-overrides)))
         (erc-format-message key ?p pfx ?n (erc--speakerize-nick nick)
-                            ?m message))
+                            ?m (erc--ensure-offset-prop message)))
     (propertize (concat "> " message) 'font-lock-face 'erc-input-face)))
 
 (defun erc-echo-notice-in-default-buffer (s parsed buffer _sender)
@@ -6881,12 +6909,12 @@ erc-ctcp-query-ACTION
            (if selfp
                (if stsmsg 'ctcp-action-statusmsg-input 'ctcp-action-input)
              (if stsmsg 'ctcp-action-statusmsg 'ctcp-action))
-           ?s (or stsmsg to)
+           ?s (or stsmsg "")
            ?p (or (and (erc-channel-user-p prefix)
                        (erc-get-channel-membership-prefix prefix))
                   "")
            ?n (erc--speakerize-nick nick dispnm)
-           ?m s))))))
+           ?m (erc--ensure-offset-prop s)))))))
 
 (defvar erc-ctcp-query-CLIENTINFO-hook '(erc-ctcp-query-CLIENTINFO))
 
@@ -7869,6 +7897,7 @@ erc-make-notice
   "Notify the user of MESSAGE."
   (when erc-minibuffer-notice
     (message "%s" message))
+  (erc--add-msg-prop 'erc--offset (length erc-notice-prefix))
   (erc-highlight-notice (concat erc-notice-prefix message)))
 
 (defun erc-highlight-error (s)
@@ -8368,8 +8397,9 @@ erc-display-msg
         (insert (erc--format-speaker-input-message line) "\n")
         (save-restriction
           (narrow-to-region insert-position (point))
-          (run-hooks 'erc-send-modify-hook)
-          (run-hooks 'erc-send-post-hook)
+          (erc--with-offset-marker
+           (run-hooks 'erc-send-modify-hook)
+           (run-hooks 'erc-send-post-hook))
           (cl-assert (> (- (point-max) (point-min)) 1))
           (add-text-properties (point-min) (1+ (point-min))
                                (erc--order-text-properties-from-hash
-- 
2.48.1

