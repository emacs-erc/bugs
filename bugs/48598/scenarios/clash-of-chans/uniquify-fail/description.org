
:PROPERTIES:
:Source: N/A
:Originator: N/A
:Subject: Unpredictable intermingling of proxied channels
:Date: N/A
:Proxy: true
:Playback: true
:END:

* Description
Remove the =autojoin= module, which is enabled by default. Set the
variable ~erc-reuse-buffers~ to =nil=. Connect to two networks via the
same bouncer.

The bouncer sends "playback" for two channels, one from each network.
The channels share the same name, and the bouncer is still subscribed to
both. A single channel buffer displaying intermingled output is created.
The process returned by ~get-buffer-process~ toggles back and forth
between client processes. This happens whenever a speaker from the "out
of phase" network speaks.

Now, emitting a =PART= in this unified buffer only leaves one network's
channel (call it "foonet's" channel). And a new channel buffer appears
exclusive to the /surviving/ network's channel ("barnet's" channel). The
other, previously intermingled buffer is left inactive. However,
attempting a =/join #chan= from its input prompt succeeds in reviving
it, meaning the same buffer is reused here (for new output exclusive to
foonet) despite ~erc-resuse-buffers~ being =nil=.

Revisiting the other =#chan= buffer and emitting a =PART= succeeds in
leaving the channel. However, a subsequent =/join #chan= in the same
buffer does not create a new buffer. Instead, it causes intermingling to
resume in the /other/ channel buffer, the one originally intermingled
and then foonet-only.

This bug is a mashup of the other two "clash" scenarios, but it exhibits
slightly different reuse behavior.
