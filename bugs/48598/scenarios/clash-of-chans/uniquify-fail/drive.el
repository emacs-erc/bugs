;; -*- lexical-binding: t; -*-

(require 'repro)

(ert-deftest repro-test ()
  (repro-with-inferior

   (repro-term-speak 3 "Going in 3 ...")

   (repro-execute-bug-recipe)

   (ert-info ("Watch #chan for a bit, part")
     (repro-term-await-buffer "#chan@foonet" 20)
     (repro-term-speak 4 "This is #chan@foonet AND #chan@barnet")
     (repro-term-speak 5 "Its buffer name lacks an <n> suffix: %S"
                       '(buffer-name))
     (repro-term-send "\e>/part")
     (sleep-for 0.5)
     (repro-term-await-buffer "#chan@barnet" 8)
     (repro-term-await-buffer "#chan@foonet" 8)
     (repro-term-send "\r")
     (repro-term-await-content "You have left" 4))

   (ert-info ("Another buffer created for #chan@barnet")
     (repro-term-await-buffer "#chan@barnet" 16)
     (repro-term-send "\C-xo")
     (repro-term-speak 4 "The new buffer (lower) is exclusive to #chan@barnet")
     (repro-term-speak 5 "Its name ends in a <2>: %S" '(buffer-name)))

   (ert-info ("Rejoin #chan@foonet")
     (repro-term-send "\C-xo")
     (repro-term-sleep-n-send 4 "/join #chan" 2 "\r")
     (repro-term-speak 4 "The top buffer is now exclusive to #chan@foonet")
     (repro-term-speak 4 "But its name is unchanged: %S" '(buffer-name)))

   (ert-info ("Trigger failure")
     (repro-term-send "\C-xo")
     (repro-term-sleep-n-send 2 "\e>/part" 4 "\r")
     (repro-term-await-content "You have left" 4)
     (repro-term-sleep-n-send 3 "/join #chan" 2 "\r")
     (repro-term-await-content "was created on" 4)
     (repro-term-await-buffer "#chan@foonet" 4)
     (repro-term-send "\C-xo")
     (repro-term-speak 5 "Both windows display the same buffer: %S"
                       '(buffer-name))
     (repro-term-speak 5 "With everything intermingled as before")
     (repro-term-send "\C-x1"))

   (ert-info ("Reckon")
     (sleep-for 5)
     (repro-term-ibuffer)
     (repro-term-speak 7 "There are two servers and two chans (1 dead)"))))
