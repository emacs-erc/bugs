#!/bin/bash
set -e

# We're in project root
[[ -f .gitlab-ci.yml ]]

bash "scripts/create_pod.bash" create_pod
bash "scripts/create_pod.bash" create_oragono foo 6667
bash "scripts/create_pod.bash" create_oragono bar 6668
bash "scripts/create_pod.bash" create_ircbot alice 6667 1/4 chan
bash "scripts/create_pod.bash" create_ircbot bob 6667 2/4 chan
bash "scripts/create_pod.bash" create_ircbot joe 6668 3/4 chan
bash "scripts/create_pod.bash" create_ircbot mike 6668 4/4 chan
bash "scripts/create_pod.bash" create_znc bugs/48598/scenarios/clash-of-chans/bouncer-history/znc
