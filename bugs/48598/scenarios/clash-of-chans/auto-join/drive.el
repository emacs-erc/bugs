;; -*- lexical-binding: t; -*-

(require 'repro)

(ert-deftest repro-test ()
  (repro-with-inferior

   (repro-term-speak 3 "Going in 3 ...")

   (repro-execute-bug-recipe)

   (ert-info ("Wait for foo")
     (repro-term-await-buffer "foonet" 20)
     (repro-term-speak 3 "This is foonet: %S" '(buffer-name)))

   (ert-info ("Join #chan@foonet")
     (repro-term-await-buffer "#chan@foonet" 8)
     (repro-term-speak 5 "Output is exclusive to #chan@foonet")
     (repro-term-speak 4 "Autojoin has us down as %S"
                       'erc-autojoin-channels-alist))

   (ert-info ("Quit foonet, join barnet")
     (repro-term-await-content "ERC: CLOSED" 3)
     (sleep-for 2)
     (repro-term-speak 5 "Connected to barnet: %S"
                       '(erc-buffer-list nil erc-server-process))
     (repro-term-speak 5 "#chan is auto-joined"))

   (ert-info ("Reconnect foonet")
     (repro-term-speak 5 "Connected to foonet: %S"
                       '(erc-buffer-list nil erc-server-process))
     (repro-term-speak 8 "Output is intermingled"))

   (ert-info ("Reckon")
     (repro-term-ibuffer)
     (repro-term-speak 5 "There are two servers and one #chan"))

   (sleep-for 2)))
