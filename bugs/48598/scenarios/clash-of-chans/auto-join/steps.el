(require 'erc-join)
(setq erc-autojoin-delay 1)

(cl-macrolet ((at (d &rest b) `(run-at-time ,d nil (lambda () (progn ,@b)))))

  (pop-to-buffer-same-window (erc :server "127.0.0.1"
                                  :port 6670
                                  :nick "tester"
                                  :password "tester@vanilla/foonet:changeme"
                                  :full-name "tester"))

  (at 4 (execute-kbd-macro "/join #chan"))
  (at 6 (execute-kbd-macro "\r"))

  (at 14 (execute-kbd-macro "\e>/quit"))
  (at 16 (execute-kbd-macro "\r"))

  (at 18 (pop-to-buffer-same-window
          (erc :server "127.0.0.1"
               :port 6670
               :nick "tester"
               :password "tester@vanilla/barnet:changeme"
               :full-name "tester")))

  (at 25 (pop-to-buffer-same-window
          (erc :server "127.0.0.1"
               :port 6670
               :nick "tester"
               :password "tester@vanilla/foonet:changeme"
               :full-name "tester"))))
