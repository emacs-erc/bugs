;; -*- lexical-binding: t; -*-

(require 'repro)

(ert-deftest repro-test ()
  (repro-with-inferior

   (repro-term-speak 3 "Going in 3 ...")

   (repro-execute-bug-recipe)

   (ert-info ("Watch #chan for a bit, part")
     (repro-term-await-buffer "#chan@foonet" 20)
     (repro-term-speak 4 "This is #chan@foonet AND #chan@barnet")
     (repro-term-speak 5 "Its buffer name has no <n> suffix: %S"
                       '(buffer-name))
     (repro-term-await-buffer "#chan@barnet" 8)
     (repro-term-send "\e>/part")
     (repro-term-await-buffer "#chan@foonet" 8)
     (repro-term-send "\r")
     (repro-term-await-content "You have left" 4))

   (ert-info ("Wait for #chan@barnet to pop up, part")
     (repro-term-await-buffer "#chan@barnet" 8)
     (repro-term-send "\C-xo")
     (repro-term-speak 4 "The new buffer (lower) is exclusive to #chan@barnet")
     (repro-term-speak 5 "Its name ends in a <2>: %S" '(buffer-name))
     (repro-term-sleep-n-send 2 "\e>/part" 2 "\r")
     (repro-term-await-content "You have left" 4)
     (repro-term-sleep-n-send 2 "/join #chan" 2 "\r")
     (repro-term-await-content "was created on" 4)
     (repro-term-speak 5 "This is a new buffer for #chan@barnet.")
     (repro-term-speak 5 "Its name ends in a <3>: %S" '(buffer-name)))

   (ert-info ("Wait for #chan@barnet to pop up")
     (repro-term-send "\C-xo")
     (repro-term-sleep-n-send 2 "/join #chan" 2 "\r")
     (repro-term-await-content "was created on" 4)
     (repro-term-speak 5 "The output is now exclusive to #chan@foonet")
     (repro-term-speak 5 "Its buffer name hasn't changed: %S"
                       '(buffer-name)))

   (ert-info ("Reckon")
     (sleep-for 5)
     (repro-term-ibuffer)
     (repro-term-speak 7 "There are two servers and three chans"))))
