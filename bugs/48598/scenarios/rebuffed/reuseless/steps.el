(require 'erc)
(setq erc-reuse-buffers nil)

(erc :server "127.0.0.1"
     :port 6670
     :nick "tester"
     :password "tester@vanilla/foonet:changeme"
     :full-name "tester")

(run-at-time 8 nil
             #'erc
             :server "127.0.0.1"
             :port 6670
             :nick "tester"
             :password "tester@vanilla/barnet:changeme"
             :full-name "tester")


