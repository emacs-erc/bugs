
(require 'repro)

(ert-deftest repro-test ()
  (repro-with-inferior

   (repro-term-speak 3 "Going in 3 ...")

   (repro-execute-bug-recipe)

   (ert-info ("Both connections succeed")
     (repro-term-await-buffer "foonet" 8)
     (repro-term-speak 4 "This should be foonet")
     (repro-term-await-buffer "barnet" 8)
     (repro-term-speak 4 "And this should be barnet"))

   (ert-info ("Channel #foo should not exist")
     (should (equal "nil" (repro-term-eval '(get-buffer "#foo")))))

   (ert-info ("2 servers, 1 buffer")
     (repro-term-list-processes)
     (repro-term-speak 5 "There should be 2 server procs for the same buffer"))

   (ert-info ("Reckon")
     (sleep-for 3)
     (repro-term-ibuffer)
     (repro-term-speak 5 "There should only be one server buffer (barnet)"))))
