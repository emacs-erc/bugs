
(require 'repro)

(ert-deftest repro-test ()
  (repro-with-inferior

   (repro-term-speak 3 "This should be steps.el")

   (repro-execute-bug-recipe)

   (ert-info ("Watch #bar for a bit")
     (repro-term-await-buffer "#bar" 10)
     (sleep-for 2)
     (repro-term-speak 8 "This should be #bar (#bar@barnet)")
     (repro-term-speak 5 "There should be an error message or two"))

   (ert-info ("Switch to server buffer")
     (repro-term-eval '(switch-to-buffer (process-buffer erc-server-process)))
     (repro-term-await-buffer "barnet" 8)
     (repro-term-speak 5 "This should be barnet with %s" 'erc-server-process))

   (ert-info ("Channel #foo should not exist")
     (should (equal "nil" (repro-term-eval '(get-buffer "#foo")))))

   (ert-info ("There are two open servers but one buffer")
     (repro-term-list-processes)
     (repro-term-speak 5 "There should be 2 server procs for the same buffer"))

   (ert-info ("Reckon")
     (repro-term-eval '(ibuffer t))
     (sleep-for 1)
     (repro-term-send "\C-xo\e>/quit")
     (sleep-for 1)
     (repro-term-send "\r")
     (sleep-for 1)
     (repro-term-speak 5 "There should be one server buffer and no #foo")
     (repro-term-send "\C-xog")
     (repro-term-speak 5 "The server process has been replaced"))))
