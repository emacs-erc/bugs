
(require 'repro)

(ert-deftest repro-test ()

  (repro-with-inferior
   (repro-execute-bug-recipe)

   (ert-info ("Wait for some buffers")
     (repro-term-await-buffer "steps.el" 2)
     (repro-term-await-buffer "my-buffer" 2))

   (ert-info ("Task completed")
     (repro-term-await-message "This is my-buffer" 5))

   (ert-info ("Say")
     (should (equal "42 (#o52, #x2a, ?*)" (repro-term-eval 42)))
     (repro-term-eval '(repro-i-speak 2 "This is the echo area")))

   (sleep-for 2)))
