
(require 'repro)

(ert-deftest repro-test ()
  (repro-with-inferior

   (repro-term-speak 3 "Going in 3 ...")

   (repro-execute-bug-recipe)

   (ert-info ("Join #chan@foonet")
     (repro-term-await-buffer "#chan@foonet" 10)
     (repro-term-await-content "#chan was created on" 10))

   (ert-info ("Change fill function")
     (sleep-for 4)
     (repro-term-speak 4 "(setq erc-fill-function #'erc-fill-static) -> %S"
                       '(setq erc-fill-function #'erc-fill-static))
     (repro-term-speak 6 "Fill should have gone static"))

   (ert-info ("Change padding")
     (repro-term-speak 4 "(setq erc-fill-static-center 20) -> %S"
                       '(setq erc-fill-static-center 20))
     (repro-term-speak 6 "Left padding should have shrunk"))

   (repro-term-eval '(ibuffer))
   (sleep-for 1)
   (term-send-raw-string "g")
   (repro-term-speak 4 "Done")

   (sleep-for 2)))
