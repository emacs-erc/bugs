(require 'erc)

(cl-macrolet ((at (d &rest b) `(run-at-time ,d nil (lambda () (progn ,@b)))))

  (pop-to-buffer-same-window (erc :server "127.0.0.1"
                                  :port 6667
                                  :nick "tester"
                                  :password "changeme"
                                  :full-name "tester"))

  (at 3 (execute-kbd-macro "/join #chan"))
  (at 5 (execute-kbd-macro "\r")))
