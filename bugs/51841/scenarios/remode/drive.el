
(require 'repro)

(ert-deftest repro-test ()
  (repro-with-inferior

   (repro-term-speak 3 "Going in 3 ...")

   (repro-execute-bug-recipe)

   (ert-info ("Join #chan@foonet")
     (repro-term-await-buffer "#chan@foonet" 10)
     (repro-term-await-content "#chan was created on" 10))

   (ert-info ("Call erc-mode in live channel buffer")
     (sleep-for 3)
     (repro-term-sleep-n-send 4 "\ex" 1 "erc-mode" 3 "\r")
     (repro-term-await-buffer "#chan/" 5)
     (sleep-for 3)
     (repro-term-speak 5 "(buffer-name) -> %S" '(buffer-name))
     (repro-term-speak 6 "This buffer is dead")
     (repro-term-sleep-n-send 0 "\e>hi" 2 "\r"))

   (ert-info ("Inspect dead one")
     (sleep-for 4)
     (repro-term-send "\C-xo\r")
     (repro-term-speak 6 "This one was recreated")
     (repro-term-speak 5 "(buffer-name) -> %S" '(buffer-name))
     (repro-term-sleep-n-send 0 "\e>bob: hi" 2 "\r")
     (sleep-for 6))

   (repro-term-eval '(ibuffer))
   (sleep-for 1)
   (term-send-raw-string "g")
   (repro-term-speak 4 "Done")

   (sleep-for 2)))
